<?php

return [
    'baseDir' => dirname(__DIR__),
    'database' => [
        'dsn' => sprintf(
            'mysql:host=%s;port=%s;dbname=%s;charset=utf8',
            getenv('MYSQL_HOST'),
            getenv('MYSQL_PORT'),
            getenv('MYSQL_DATABASE')
        ),
        'username' => getenv('MYSQL_USER'),
        'password' => getenv('MYSQL_PASSWORD'),
    ],
    'session' => [
        'save_handler' => 'memcached',
        'save_path' => implode(':', [getenv('MEMCACHED_ADDRESS'), getenv('MEMCACHED_PORT')]),
        'cookie_lifetime' => 60 * 60 * 30,
    ],
    'i18n' => [
        'messages' => [
            'ru' => require __DIR__ . '/messages_ru.php',
            'en' => require __DIR__ . '/messages_en.php',
        ],
        'lang' => 'ru',
    ],
    'identity' => [
        'class' => 'TRLogic\Models\Identity',
    ],
];
