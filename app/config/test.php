<?php

$config = require __DIR__  . '/web.php';
$config['database']['dsn'] = sprintf(
    'mysql:host=%s;port=%s;dbname=%s;charset=utf8',
    getenv('MYSQL_HOST'),
    getenv('MYSQL_PORT'),
    getenv('MYSQL_TEST_DATABASE')
);

return $config;
