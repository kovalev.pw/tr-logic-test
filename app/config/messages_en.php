<?php

return [
    'LANGUAGE_NAME' => 'English',

    'LABEL_EMAIL' => 'Email',
    'LABEL_PASSWORD' => 'Password',
    'LABEL_PASSWORD_REPEAT' => 'Password repeat',
    'LABEL_AVATAR' => 'Avatar',
    'LABEL_FULL_NAME' => 'Your name',
    'LABEL_CREATED_AT' => 'Date of registration',

    'PLACEHOLDER_EMAIL' => 'user@domain',
    'PLACEHOLDER_PASSWORD' => 'A-z0-9',
    'PLACEHOLDER_PASSWORD_REPEAT' => 'A-z0-9',
    'PLACEHOLDER_AVATAR' => 'No file selected',
    'PLACEHOLDER_FULL_NAME' => 'Thomas Anderson',

    'ACTION_LOGIN' => 'Login',
    'ACTION_REGISTRATION' => 'Registration',
    'ACTION_PROFILE' => 'Profile',

    'MESSAGE_REGISTRATION_COMPLETE' => 'Registration completed! Now you can enter
using the email and password specified during registration.',

    'MODAL_LOGIN' => 'Sign in',
    'MODAL_REGISTRATION' => 'Sign up',

    'BUTTON_CANCEL' => 'Cancel',
    'BUTTON_LOGIN' => 'Sign in',
    'BUTTON_REGISTRATION' => 'Sign up',

    'ERROR_INVALID_EMAIL' => 'The value entered is not a valid email address.',
    'ERROR_INVALID_PASSWORD' => 'Invalid password.',
    'ERROR_PASSWORD_FORMAT' => 'Only latin letters, numbers and special characters are allowed.',
    'ERROR_EMAIL_ALREADY_REGISTERED' => 'The specified email is already registered.',
    'ERROR_PASSWORD_LENGTH' => 'Password must be between %1$d and %2$d characters.',
    'ERROR_EMAIL_NOT_FOUND' => 'The specified email is not registered.',
    'ERROR_COMPARE_PASSWORDS' => 'Passwords do not match.',
    'ERROR_INVALID_NAME' => 'Invalid value format. Letters, spaces and the «-» sign are allowed.',
    'ERROR_NAME_LENGTH' => 'The name must be between %1$d and %2$d characters.',
    'ERROR_FILE_SIZE' => 'File size exceeded: %1$01.2F %2$s.',
    'ERROR_FILE_UPLOAD' => 'File upload failed. Try again.',
    'ERROR_FILE_TYPE' => 'This file type is not supported.',
    'ERROR_INVALID_LANGUAGE' => 'Invalid language.',
];
