<?php

return [
    'LANGUAGE_NAME' => 'Русский',

    'LABEL_EMAIL' => 'Email',
    'LABEL_PASSWORD' => 'Пароль',
    'LABEL_PASSWORD_REPEAT' => 'Пароль снова',
    'LABEL_AVATAR' => 'Аватар',
    'LABEL_FULL_NAME' => 'Ваше имя',
    'LABEL_CREATED_AT' => 'Дата регистрации',

    'PLACEHOLDER_EMAIL' => 'user@domain',
    'PLACEHOLDER_PASSWORD' => 'A-z0-9',
    'PLACEHOLDER_PASSWORD_REPEAT' => 'A-z0-9',
    'PLACEHOLDER_AVATAR' => 'Файл не выбран',
    'PLACEHOLDER_FULL_NAME' => 'Иванов Иван',

    'ACTION_LOGIN' => 'Войти',
    'ACTION_REGISTRATION' => 'Регистрация',
    'ACTION_PROFILE' => 'Профиль',

    'MESSAGE_REGISTRATION_COMPLETE' => 'Регистрация завершена! Теперь Вы можете войти,
используя email и пароль указанные при регистрации.',

    'MODAL_LOGIN' => 'Войти',
    'MODAL_REGISTRATION' => 'Зарегистрироваться',

    'BUTTON_CANCEL' => 'Отмена',
    'BUTTON_LOGIN' => 'Войти',
    'BUTTON_REGISTRATION' => 'Зарегистрироваться',

    'ERROR_INVALID_EMAIL' => 'Введённое значение не является правильным email адресом.',
    'ERROR_INVALID_PASSWORD' => 'Неверный пароль.',
    'ERROR_PASSWORD_FORMAT' => 'Разрешены только латинские буквы, цифры и специальные символы.',
    'ERROR_EMAIL_ALREADY_REGISTERED' => 'Указанный email уже зарегистрирован.',
    'ERROR_PASSWORD_LENGTH' => 'Длина пароля должна быть от %1$d до %2$d символов.',
    'ERROR_EMAIL_NOT_FOUND' => 'Указанный email не зарегистрирован.',
    'ERROR_COMPARE_PASSWORDS' => 'Пароли не совпадают.',
    'ERROR_INVALID_NAME' => 'Неверный формат значения. Разрешены буквы, пробелы и знак «-».',
    'ERROR_NAME_LENGTH' => 'Длина имени должна быть от %1$d до %2$d символов.',
    'ERROR_FILE_SIZE' => 'Превышен допустимый размер файла: %1$01.2F %2$s.',
    'ERROR_FILE_UPLOAD' => 'Загрузка файла не удалась. Повторите попытку.',
    'ERROR_FILE_TYPE' => 'Данный тип файла не поддерживается.',
    'ERROR_INVALID_LANGUAGE' => 'Invalid language.',
];
