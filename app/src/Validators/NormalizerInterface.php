<?php

namespace TRLogic\Validators;

/**
 * Interface NormalizerInterface
 *
 * Интерфейс нормализатора значений формы
 */
interface NormalizerInterface
{
    /**
     * Привести значение к нормальному виду
     *
     * @param mixed $value Входное значение
     * @return string|mixed Вернуть обработанное значение
     */
    public function normalizeValue($value);
}
