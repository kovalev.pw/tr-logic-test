<?php

namespace TRLogic\Validators;

/**
 * Class RegexValidator
 *
 * Выполняет проверку значения на соответствие шаблону регулярного выражения
 */
class RegexValidator extends AbstractValidator implements ValidatorDataInterface
{
    /**
     * @var string Шаблон регулярного выражения
     */
    private $rule;

    /**
     * RegexValidator constructor.
     *
     * @param string $errorMessage Описание ошибки
     * @param string $rule Шаблон регулярного выражения
     */
    public function __construct(string $errorMessage, string $rule)
    {
        $this->rule = $rule;

        parent::__construct($errorMessage);
    }

    /**
     * @inheritDoc
     */
    public function getValidatorData() : array
    {
        return [$this->errorMessage, $this->rule];
    }

    /**
     * @inheritDoc
     */
    public function validateValue($value) : bool
    {
        if (!is_scalar($value) || !preg_match($this->rule, $value)) {
            return false;
        }

        return true;
    }
}
