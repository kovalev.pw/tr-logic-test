<?php

namespace TRLogic\Validators;

/**
 * Trait ModelVerifier
 *
 * Выполняет проверку полей модели / формы
 */
class ModelVerifier
{
    /**
     * @var ModelInterface
     */
    private $model;

    /**
     * ValidatorComponent constructor.
     *
     * @param ModelInterface $model Проверяемая модель
     */
    public function __construct(ModelInterface $model)
    {
        $this->model = $model;
    }

    /**
     * Выполнить загрузку данных формы
     *
     * @param string[] $data Массив данных формы из POST или GET
     * @return void
     */
    public function loadData(array $data) : void
    {
        $attributes = $this->model->getAttributes();

        array_walk($attributes, function (string $attribute) use ($data) {
            if (array_key_exists($attribute, $data)) {
                $this->model->$attribute = $data[$attribute];
            }
        });
    }

    /**
     * Проверить форму
     *
     * @return bool Вернуть TRUE в случае успеха, иначе FALSE
     */
    public function validateModel() : bool
    {
        $normalizers = $this->model->getNormalizers();
        $validators = $this->model->getValidators();

        array_walk($normalizers, [$this, 'normalizeAttribute']);
        array_walk($validators, [$this, 'validateAttribute']);

        return $this->model->getErrors()->isEmpty();
    }

    /**
     * Выполнить нормализацию значения атрибута
     *
     * @param NormalizerInterface $normalizer Нормализатор
     * @param string $attribute Имя атрибута
     * @return void
     */
    private function normalizeAttribute(NormalizerInterface $normalizer, string $attribute) : void
    {
        $this->model->$attribute = $normalizer->normalizeValue($this->model->$attribute);
    }

    /**
     * Выполнить валидацию значения атрибута
     *
     * @param ValidatorInterface $validator Валидатор
     * @param string $attribute Имя атрибута
     * @return void
     */
    private function validateAttribute(ValidatorInterface $validator, string $attribute) : void
    {
        if (!$validator->validateValue($this->model->$attribute)) {
            $this->model->getErrors()->setItem($attribute, $validator->getErrorMessage());
        }
    }
}
