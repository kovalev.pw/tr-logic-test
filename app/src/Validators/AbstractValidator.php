<?php

namespace TRLogic\Validators;

/**
 * Class AbstractValidator
 */
abstract class AbstractValidator implements ValidatorInterface
{
    /**
     * @var string Описание ошибки
     */
    protected $errorMessage;

    /**
     * AbstractValidator constructor.
     *
     * @param string $errorMessage Описание ошибки
     */
    public function __construct(string $errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * @inheritDoc
     */
    public function getErrorMessage() : string
    {
        return $this->errorMessage;
    }
}
