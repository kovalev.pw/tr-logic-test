<?php

namespace TRLogic\Validators;

/**
 * Class EmailNormalizer
 *
 * Выполняет приведение email-адреса к нижнему регистру
 */
class EmailNormalizer implements NormalizerInterface
{
    /**
     * @inheritDoc
     */
    public function normalizeValue($value)
    {
        if (!is_null($value) && !is_scalar($value)) {
            return $value;
        }

        return strtolower($value);
    }
}
