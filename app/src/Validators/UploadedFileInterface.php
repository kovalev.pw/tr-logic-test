<?php

namespace TRLogic\Validators;

/**
 * Interface UploadedFileInterface
 *
 * Интерфейс временного файла, загруженного на сервер
 */
interface UploadedFileInterface
{
    /**
     * Копирует данные из временного файла
     *
     * @param string $destinationPath Адрес файла назначения
     * @return void
     */
    public function copyToDestination(string $destinationPath) : void;

    /**
     * Получить код ошибки загрузки файла
     *
     * @return int Вернуть значение, соответствующее одной из констант:
     * @see UPLOAD_ERR_OK
     * @see UPLOAD_ERR_INI_SIZE
     * @see UPLOAD_ERR_FORM_SIZE
     * @see UPLOAD_ERR_PARTIAL
     * @see UPLOAD_ERR_NO_FILE
     * @see UPLOAD_ERR_NO_TMP_DIR
     * @see UPLOAD_ERR_CANT_WRITE
     * @see UPLOAD_ERR_EXTENSION
     */
    public function getError() : int;

    /**
     * Получить mime-тип файла
     *
     * @return string
     * @see mime_content_type()
     */
    public function getMimeType() : string;

    /**
     * Получить размер загруженного файла
     *
     * @return int Количество байт
     */
    public function getSize() : int;

    /**
     * Определить существование ошибки, возникшей при загрузки файла
     *
     * @return bool Вернуть TRUE - если есть ошибка, иначе FALSE
     * @see UPLOAD_ERR_OK
     */
    public function hasError() : bool;

    /**
     * Определить, был ли файл отправлен клиентом
     *
     * @return bool
     */
    public function isFileReceived() : bool;
}
