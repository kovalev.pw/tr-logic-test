<?php

namespace TRLogic\Validators;

use ReflectionClass;

/**
 * Class FrontendAdapter
 *
 * Преобразует данные валидаторов модели в один массив
 */
class FrontendAdapter
{
    /**
     * @var ModelInterface
     */
    private $model;

    /**
     * FrontendAdapter constructor.
     *
     * @param ModelInterface $model
     */
    public function __construct(ModelInterface $model)
    {
        $this->model = $model;
    }

    /**
     * Получить данные валидаторов формы
     *
     * @return mixed[][]
     */
    public function getValidatorsData() : array
    {
        $validators = $this->model->getValidators();
        $data = [];

        array_walk($validators, function (ValidatorInterface $validator, string $attribute) use (&$data) {
            if ($validator instanceof CompositeValidatorInterface) {
                $data[$attribute] = $this->getCompositeValidatorData($validator);
            } elseif ($validator instanceof ValidatorDataInterface) {
                $data[$attribute] = [$this->getValidatorData($validator)];
            }
        });

        return array_filter($data, 'count');
    }

    /**
     * Получить данные валидаторов из компоновщика
     *
     * @param CompositeValidatorInterface $compositeValidator
     * @return mixed[][]
     */
    private function getCompositeValidatorData(CompositeValidatorInterface $compositeValidator) : array
    {
        $validators = array_filter($compositeValidator->getValidators(), function (ValidatorInterface $validator) {
            return ($validator instanceof ValidatorDataInterface);
        });

        return array_map([$this, 'getValidatorData'], array_values($validators));
    }

    /**
     * Получить данные валидатора
     *
     * @param ValidatorDataInterface $validator Экземпляр валидатора
     * @return mixed[]
     */
    private function getValidatorData(ValidatorDataInterface $validator) : array
    {
        return [self::getValidatorName($validator), $validator->getValidatorData()];
    }

    /**
     * Получить имя валидатора
     *
     * @param ValidatorInterface $validator
     * @return string Имя текущего класса без namespace
     * @throws
     */
    private static function getValidatorName(ValidatorInterface $validator) : string
    {
        $class = new ReflectionClass($validator);

        return $class->getShortName();
    }
}
