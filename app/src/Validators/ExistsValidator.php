<?php

namespace TRLogic\Validators;

/**
 * Class ExistsValidator
 *
 * Выполняет проверку значения, на присутствие в заданном списке
 */
class ExistsValidator extends AbstractValidator
{
    /**
     * @var mixed[] Список значений
     */
    private $values;

    /**
     * ExistsValidator constructor.
     *
     * @param string $errorMessage Описание ошибки
     * @param mixed[] $values Список значений
     */
    public function __construct(string $errorMessage, array $values)
    {
        $this->values = $values;

        parent::__construct($errorMessage);
    }

    /**
     * @inheritDoc
     */
    public function validateValue($value): bool
    {
        if (!in_array($value, $this->values)) {
            return false;
        }

        return true;
    }
}
