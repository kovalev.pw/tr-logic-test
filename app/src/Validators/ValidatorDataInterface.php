<?php

namespace TRLogic\Validators;

/**
 * Interface ValidatorDataInterface
 *
 * Интерфейс данных, используемых JS-валидатором
 */
interface ValidatorDataInterface extends ValidatorInterface
{
    /**
     * Получить аргументы конструктора JS-валидатора
     *
     * @return mixed[]
     */
    public function getValidatorData() : array;
}
