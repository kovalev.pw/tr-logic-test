<?php

namespace TRLogic\Validators;

/**
 * Class EmailValidator
 *
 * Валидатор email-адресов
 */
class EmailValidator extends AbstractValidator
{
    /**
     * @inheritDoc
     */
    public function validateValue($value) : bool
    {
        if (!is_scalar($value) || !filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        return true;
    }
}
