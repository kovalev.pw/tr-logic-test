<?php

namespace TRLogic\Validators;

/**
 * Class CompareValidator
 *
 * Выполняет сравнение двух значений
 */
class CompareValidator extends AbstractValidator implements ValidatorDataInterface
{
    /**
     * @var string Эталонный атрибут формы
     */
    private $targetAttribute;

    /**
     * @var mixed Эталон значения
     */
    private $value;

    /**
     * CompareValidator constructor.
     *
     * @param string $errorMessage Описание ошибки
     * @param string $targetAttribute Эталонный атрибут формы
     * @param mixed $value Эталон значения
     */
    public function __construct(string $errorMessage, string $targetAttribute, $value)
    {
        $this->value = $value;
        $this->targetAttribute = $targetAttribute;

        parent::__construct($errorMessage);
    }

    /**
     * @inheritDoc
     */
    public function getValidatorData() : array
    {
        return [$this->errorMessage, $this->targetAttribute];
    }

    /**
     * @inheritDoc
     */
    public function validateValue($value) : bool
    {
        return ($this->value === $value);
    }
}
