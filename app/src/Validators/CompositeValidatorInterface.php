<?php

namespace TRLogic\Validators;

/**
 * Interface CompositeValidatorInterface
 *
 * Интерфейс компоновщика валидаторов
 */
interface CompositeValidatorInterface extends ValidatorInterface
{
    /**
     * Получить коллекцию валидаторов
     *
     * @return ValidatorInterface[]
     */
    public function getValidators() : array;
}
