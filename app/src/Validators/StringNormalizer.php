<?php

namespace TRLogic\Validators;

/**
 * Class StringNormalizer
 *
 * Выполняет удаление лишних пробельных символов из значения
 */
class StringNormalizer implements NormalizerInterface
{
    /**
     * @inheritDoc
     */
    public function normalizeValue($value)
    {
        if (!is_null($value) && !is_scalar($value)) {
            return $value;
        }

        return trim(preg_replace('/\s+/', ' ', $value));
    }
}
