<?php

namespace TRLogic\Validators;

use TRLogic\Collections\CollectionInterface;

/**
 * Interface ModelInterface
 *
 * Интерфейс модели
 */
interface ModelInterface
{
    /**
     * Получить список атрибутов формы
     *
     * @return string[]
     */
    public function getAttributes() : array;

    /**
     * Получить список нормализаторов формы
     *
     * @return NormalizerInterface[]
     */
    public function getNormalizers() : array;

    /**
     * Получить список валидаторов формы
     *
     * @return ValidatorInterface[]
     */
    public function getValidators() : array;

    /**
     * Получить экземпляр коллекции ошибок
     *
     * @return CollectionInterface|string[]
     */
    public function getErrors() : CollectionInterface;
}
