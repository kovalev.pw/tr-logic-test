<?php

namespace TRLogic\Validators;

/**
 * Interface ValidatorInterface
 *
 * Интерфейс валидатора значений формы
 */
interface ValidatorInterface
{
    /**
     * Получить описание ошибки
     *
     * @return string
     */
    public function getErrorMessage() : string;

    /**
     * Проверить значение
     *
     * @param mixed $value Проверяемое значение
     * @return bool Вернуть TRUE в случае успеха, иначе FALSE
     */
    public function validateValue($value) : bool;
}
