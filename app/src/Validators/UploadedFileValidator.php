<?php

namespace TRLogic\Validators;

/**
 * Class UploadedFileValidator
 *
 * Выполняет проверку файла, загруженного на сервер
 */
class UploadedFileValidator extends AbstractValidator implements ValidatorDataInterface
{
    /**
     * @var string Описание ошибки неудачной загрузки
     */
    private $errorUploadMessage;

    /**
     * @var string Описание ошибки несоответствия mime-типа
     */
    private $invalidTypeMessage;

    /**
     * @var string Описание ошибки размера
     */
    private $invalidSizeMessage;

    /**
     * @var int Максимальный размер файла, байт
     */
    private $maxFileSize;

    /**
     * @var string[] Список разрешённых типов
     */
    private $mimeTypes;

    /**
     * UploadedFileValidator constructor.
     *
     * @param string $errorUploadErrorMessage Описание ошибки неудачной загрузки
     * @param string $invalidTypeMessage Описание ошибки несоответствия mime-типа
     * @param string $invalidSizeMessage Описание ошибки размера
     * @param array $mimeTypes Список разрешённых типов
     * @param int|null $maxFileSize Максимальный размер файла, байт
     */
    public function __construct(
        string $errorUploadErrorMessage,
        string $invalidTypeMessage,
        string $invalidSizeMessage,
        array $mimeTypes,
        int $maxFileSize = null
    ) {
        $this->errorUploadMessage = $errorUploadErrorMessage;

        $this->mimeTypes = $mimeTypes;
        $this->invalidTypeMessage = $invalidTypeMessage;

        $this->maxFileSize = min($maxFileSize ?: PHP_INT_MAX, self::getMaxFileSize());
        $this->invalidSizeMessage = self::formatBytes($invalidSizeMessage, $this->maxFileSize);

        parent::__construct('');
    }

    /**
     * @inheritDoc
     */
    public function getValidatorData() : array
    {
        return [$this->invalidTypeMessage, $this->invalidSizeMessage, $this->mimeTypes, $this->maxFileSize];
    }

    /**
     * @inheritDoc
     * @param UploadedFileInterface $value Загруженный файл
     */
    public function validateValue($value) : bool
    {
        return $this->validateUploadedFile($value);
    }

    /**
     * Отформатировать сообщение о максимально допустимом размере файла
     *
     * @param string $format Формат сообщения
     * @param int $bytes Количество байт
     * @return string Вернуть отформатированную строку
     */
    private static function formatBytes(string $format, int $bytes) : string
    {
        $base = log($bytes, 1024);
        $floor = (int) floor($base);
        $suffixes = ['B', 'KiB', 'MiB', 'GiB'];
        $suffix = $suffixes[$floor];

        return sprintf($format, pow(1024, $base - $floor), $suffix);
    }

    /**
     * Получить максимально допустимый размер загружаемого файла из системной конфигурации PHP
     *
     * @return int Количество байт
     */
    private static function getMaxFileSize() : int
    {
        $value = strtoupper(ini_get('upload_max_filesize'));
        $offset = array_search(substr($value, -1), ['K', 'M', 'G']);
        $value = (float) substr($value, 0, -1);

        return $value * pow(1024, $offset + 1);
    }

    /**
     * Проверить загруженный файл
     *
     * @inheritDoc
     * @param UploadedFileInterface $value Загруженный файл
     */
    private function validateUploadedFile(UploadedFileInterface $uploadedFile) : bool
    {
        if (!$uploadedFile->isFileReceived()) {
            return true;
        }

        if (!$this->validateSize($uploadedFile)) {
            $this->errorMessage = $this->invalidSizeMessage;

            return false;
        }

        if ($uploadedFile->hasError()) {
            $this->errorMessage = $this->errorUploadMessage;

            return false;
        }

        if (!$this->validateType($uploadedFile)) {
            $this->errorMessage = $this->invalidTypeMessage;

            return false;
        }

        return true;
    }

    /**
     * Проверить размер файла
     *
     * @param UploadedFileInterface $uploadedFile Загруженный файл
     * @return bool Вернуть TRUE в случае успеха, иначе FALSE
     */
    private function validateSize(UploadedFileInterface $uploadedFile) : bool
    {
        $hasError = in_array($uploadedFile->getError(), [UPLOAD_ERR_INI_SIZE, UPLOAD_ERR_FORM_SIZE]);

        if ($hasError || $uploadedFile->getSize() > $this->maxFileSize) {
            return false;
        }

        return true;
    }

    /**
     * Проверить mime-тип файла
     *
     * @param UploadedFileInterface $uploadedFile Загруженный файл
     * @return bool Вернуть TRUE в случае успеха, иначе FALSE
     */
    private function validateType(UploadedFileInterface $uploadedFile) : bool
    {
        if (!in_array($uploadedFile->getMimeType(), $this->mimeTypes)) {
            return false;
        }

        return true;
    }
}
