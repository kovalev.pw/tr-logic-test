<?php

namespace TRLogic\Validators;

/**
 * Class StringLengthValidator
 *
 * Выполняет проверку длины строки значения
 */
class StringLengthValidator extends AbstractValidator implements ValidatorDataInterface
{
    /**
     * @var int Минимальная длина значения
     */
    private $minLength;

    /**
     * @var int Максимальная длина значения
     */
    private $maxLength;

    /**
     * StringValidator constructor.
     *
     * @param string $errorMessage Описание ошибки
     * @param int $minLength Минимальная длина значения
     * @param int $maxLength Максимальная длина значения
     */
    public function __construct(string $errorMessage, int $minLength, int $maxLength)
    {
        $this->minLength = $minLength;
        $this->maxLength = $maxLength;

        parent::__construct(sprintf($errorMessage, $this->minLength, $this->maxLength));
    }

    /**
     * @inheritDoc
     */
    public function getValidatorData() : array
    {
        return [$this->errorMessage, $this->minLength, $this->maxLength];
    }

    /**
     * @inheritDoc
     */
    public function validateValue($value) : bool
    {
        if (!is_scalar($value)) {
            return false;
        }

        $length = mb_strlen($value, "UTF-8");

        if ($length < $this->minLength || $length > $this->maxLength) {
            return false;
        }

        return true;
    }
}
