<?php

namespace TRLogic\Validators;

/**
 * Class CompositeValidator
 *
 * Компоновщик валидаторов
 */
class CompositeValidator extends AbstractValidator implements CompositeValidatorInterface
{
    /**
     * @var ValidatorInterface[] Коллекция валидаторов
     */
    private $validators;

    /**
     * CompositeValidator constructor.
     *
     * @param ValidatorInterface[] $validators Коллекция валидаторов
     */
    public function __construct(array $validators)
    {
        parent::__construct('');

        $this->validators = $validators;
    }

    /**
     * @inheritDoc
     */
    public function getValidators() : array
    {
        return $this->validators;
    }

    /**
     * @inheritDoc
     */
    public function validateValue($value) : bool
    {
        $this->errorMessage = '';
        $validators = $this->getValidators();

        array_walk($validators, function (ValidatorInterface $validator) use ($value) {
            if (empty($this->errorMessage) && !$validator->validateValue($value)) {
                $this->errorMessage = $validator->getErrorMessage();
            }
        });

        return empty($this->errorMessage);
    }
}
