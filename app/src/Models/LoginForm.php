<?php

namespace TRLogic\Models;

use TRLogic\Database\UserTable;
use TRLogic\Web\Application;

/**
 * Class LoginForm
 *
 * Выполняет авторизацию пользователя
 */
class LoginForm extends AbstractForm
{
    /**
     * @var string Почтовый адрес пользователя
     */
    public $email;

    /**
     * @var string Пароль пользователя
     */
    public $password;

    /**
     * Выполнить авторизацию пользователя
     *
     * @return bool Вернуть TRUE в случае успеха, иначе FALSE
     */
    public function login() : bool
    {
        $messages = Application::getInstance()->getI18n()->getMessages();
        $data = UserTable::findRow(['email' => $this->email]);

        if (empty($data)) {
            $this->getErrors()->setItem('email', $messages['ERROR_EMAIL_NOT_FOUND']);

            return false;
        }

        $identity = new Identity($data);

        if (!$identity->verifyPassword($this->password)) {
            $this->getErrors()->setItem('password', $messages['ERROR_INVALID_PASSWORD']);

            return false;
        }

        Application::getInstance()->getAuth()->signIn($identity);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getAttributes() : array
    {
        return ['email', 'password'];
    }

    /**
     * @inheritDoc
     */
    public function getNormalizers() : array
    {
        return [
            'email' => $this->validatorFactory->createEmailNormalizer(),
        ];
    }

    /**
     * @inheritDoc
     */
    public function getValidators() : array
    {
        return [
            'email' => $this->validatorFactory->createEmailValidator(),
            'password' => $this->validatorFactory->createPasswordValidator(),
        ];
    }
}
