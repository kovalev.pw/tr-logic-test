<?php

namespace TRLogic\Models;

use TRLogic\Collections\Collection;
use TRLogic\Collections\CollectionInterface;
use TRLogic\Validators\ModelInterface;

/**
 * Class AbstractForm
 */
abstract class AbstractForm implements ModelInterface
{
    /**
     * @var Collection Коллекция ошибок валидации формы
     */
    private $errors;

    /**
     * @var ValidatorFactory
     */
    protected $validatorFactory;

    /**
     * AbstractForm constructor.
     */
    public function __construct()
    {
        $this->validatorFactory = new ValidatorFactory();
        $this->errors = new Collection();
    }

    /**
     * @inheritDoc
     */
    public function getErrors() : CollectionInterface
    {
        return $this->errors;
    }
}
