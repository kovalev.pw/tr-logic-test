<?php

namespace TRLogic\Models;

use TRLogic\Database\UserTable;
use TRLogic\Web\IdentityInterface;

/**
 * Class Identity
 */
class Identity implements IdentityInterface
{
    /**
     * @var string[] Коллекция параметров пользователя
     */
    private $data = [];

    /**
     * Identity constructor.
     *
     * @param array $data Коллекция параметров пользователя
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @inheritDoc
     */
    public static function findById(int $id) : ?IdentityInterface
    {
        $data = UserTable::findRow(['user_id' => $id]);

        return empty($data) ? null : new self($data);
    }

    /**
     * @inheritDoc
     */
    public function getId() : int
    {
        return $this->data['user_id'];
    }

    /**
     * @inheritDoc
     */
    public function getData() : array
    {
        return $this->data;
    }

    /**
     * @inheritDoc
     */
    public function generateHash() : string
    {
        $values = [$this->data['user_id'], $this->data['password_hash']];

        return md5(implode(':', $values));
    }

    /**
     * Проверить пароль
     *
     * @param string $password Пароль
     * @return bool Вернуть TRUE в случае успеха, иначе FALSE
     */
    public function verifyPassword(string $password) : bool
    {
        return password_verify($password, $this->data['password_hash']);
    }
}
