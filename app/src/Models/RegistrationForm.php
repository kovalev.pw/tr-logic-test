<?php

namespace TRLogic\Models;

use TRLogic\Database\UserTable;
use TRLogic\Web\Application;

/**
 * Class RegistrationForm
 *
 * Выполняет регистрацию нового пользователя
 */
class RegistrationForm extends AbstractForm
{
    /** @var string Имя директории для загрузки аватара */
    private const AVATAR_DIR = 'avatars';

    /**
     * @var string Полное имя
     */
    public $full_name;

    /**
     * @var string Почтовый адрес
     */
    public $email;

    /**
     * @var string Пароль
     */
    public $password;

    /**
     * @var string Повтор пароля
     */
    public $password_repeat;

    /**
     * @var UploadedFile Загруженный временный файл аватара
     */
    public $avatar;

    /**
     * RegistrationForm constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $uploadsPath = Application::getInstance()->getBaseDir() . '/' . Application::UPLOADS_DIR;
        $this->avatar = new UploadedFile('avatar', $uploadsPath);
    }

    /**
     * Выполнить регистрацию нового пользователя
     *
     * @return bool Вернуть TRUE в случае успеха, иначе FALSE
     */
    public function registerUser() : bool
    {
        $messages = Application::getInstance()->getI18n()->getMessages();

        if (UserTable::findRow(['email' => $this->email])) {
            $this->getErrors()->setItem('email', $messages['ERROR_EMAIL_ALREADY_REGISTERED']);

            return false;
        }

        $user = [
            'email' => $this->email,
            'password_hash' => password_hash($this->password, PASSWORD_DEFAULT),
            'full_name' => $this->full_name,
            'created_at' => gmdate('Y-m-d H:i:s'),
        ];

        if ($this->avatar->isFileReceived()) {
            $user['avatar'] = $this->uploadAvatar();
        }

        return UserTable::insertRow($user);
    }

    /**
     * @inheritDoc
     */
    public function getAttributes() : array
    {
        return ['full_name', 'email', 'password', 'password_repeat'];
    }

    /**
     * @inheritDoc
     */
    public function getNormalizers() : array
    {
        return [
            'email' => $this->validatorFactory->createEmailNormalizer(),
            'full_name' => $this->validatorFactory->createStringNormalizer(),
        ];
    }

    /**
     * @inheritDoc
     */
    public function getValidators() : array
    {
        return [
            'email' => $this->validatorFactory->createEmailValidator(),
            'password' => $this->validatorFactory->createPasswordValidator(),
            'password_repeat' => $this->validatorFactory->createCompareValidator($this->password),
            'full_name' => $this->validatorFactory->createFullNameValidator(),
            'avatar' => $this->validatorFactory->createAvatarValidator(),
        ];
    }

    /**
     * Выполнить загрузку аватара пользователя
     *
     * @return string Вернуть имя загруженного файла
     */
    private function uploadAvatar() : string
    {
        $baseDir = Application::getInstance()->getBaseDir();
        $type = ValidatorFactory::AVATAR_MIME_TYPES[$this->avatar->getMimeType()];
        $fileName = $this->generateFileName() . '.' . $type;
        $pieces = [$baseDir, Application::PUBLIC_DIR, Application::ASSETS_DIR, self::AVATAR_DIR, $fileName];
        $destination = implode(DIRECTORY_SEPARATOR, $pieces);
        $this->avatar->copyToDestination($destination);

        return $fileName;
    }

    /**
     * Генерировать имя файла
     *
     * @return string Вернуть hex-строку из 16 символов
     */
    private function generateFileName() : string
    {
        $time = explode('.', microtime(true));

        return sprintf("%1$08x%2$08x", $time[0], $time[1] * 10000);
    }
}
