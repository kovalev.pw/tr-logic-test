<?php

namespace TRLogic\Models;

use TRLogic\Validators\UploadedFileInterface;
use Error;

/**
 * Class UploadedFile
 *
 * Предоставляет данные о загруженном файле
 */
class UploadedFile implements UploadedFileInterface
{
    private const FATAL_ERRORS = [
        UPLOAD_ERR_NO_TMP_DIR => 'UPLOAD_ERR_NO_TMP_DIR',
        UPLOAD_ERR_CANT_WRITE => 'UPLOAD_ERR_CANT_WRITE',
        UPLOAD_ERR_EXTENSION => 'UPLOAD_ERR_EXTENSION',
    ];

    /**
     * @var int Код ошибки загрузки файла
     */
    private $error = -1;

    /**
     * @var bool Определяет, был ли файл отправлен клиентом
     */
    private $isFileReceived = false;

    /**
     * @var int Размер загруженного файла
     */
    private $size = 0;

    /**
     * @var string Полное имя временного файла
     */
    private $tempName;

    /**
     * UploadedFile constructor.
     *
     * @param string $key Имя атрибута формы
     * @param string $uploadPath Адрес временной папки
     */
    public function __construct(string $key, string $uploadPath)
    {
        if (isset($_FILES[$key]['tmp_name'])) {
            $this->error = $_FILES[$key]['error'];
            $this->size = $_FILES[$key]['size'];

            if ($this->error != UPLOAD_ERR_NO_FILE) {
                $this->isFileReceived = true;
            }

            $fatalError = self::FATAL_ERRORS[$this->error] ?? null;

            if ($fatalError) {
                throw new Error("File upload error: {$fatalError}", E_USER_ERROR);
            }

            if (!$this->hasError()) {
                $this->moveUploadedFile($_FILES[$key]['tmp_name'], $uploadPath);
            }
        }
    }

    /**
     * Удаляет временный файл
     *
     * @return void
     */
    public function __destruct()
    {
        if ($this->tempName && file_exists($this->tempName)) {
            unlink($this->tempName);
        }
    }

    /**
     * @inheritDoc
     */
    public function copyToDestination(string $destinationPath) : void
    {
        if (false === @copy($this->tempName, $destinationPath)) {
            $error = error_get_last();
            $this->__destruct();

            throw new Error($error['message'], $error['type']);
        }
    }

    /**
     * @inheritDoc
     */
    public function getError() : int
    {
        return $this->error;
    }

    /**
     * @inheritDoc
     */
    public function getMimeType() : string
    {
        return mime_content_type($this->tempName);
    }

    /**
     * @inheritDoc
     */
    public function getSize() : int
    {
        return $this->size;
    }

    /**
     * @inheritDoc
     */
    public function hasError() : bool
    {
        return ($this->error !== UPLOAD_ERR_OK);
    }

    /**
     * @inheritDoc
     */
    public function isFileReceived() : bool
    {
        return $this->isFileReceived;
    }

    /**
     * Переместить загруженный файл
     *
     * @param string $tempName Полное имя загруженного файла
     * @param string $uploadsPath Директория временного файла
     * @return void
     */
    private function moveUploadedFile(string $tempName, string $uploadsPath) : void
    {
        $this->tempName = tempnam($uploadsPath, 'tmp');

        if (false === $this->tempName || false === @move_uploaded_file($tempName, $this->tempName)) {
            $error = error_get_last();
            $this->__destruct();

            throw new Error($error['message'], $error['type']);
        }
    }
}
