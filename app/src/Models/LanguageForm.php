<?php

namespace TRLogic\Models;

use TRLogic\Web\Session;

/**
 * Class LanguageForm
 *
 * Выполняет смену языка интерфейса
 */
class LanguageForm extends AbstractForm
{
    /**
     * @var string Язык интерфейса
     */
    public $lang;

    /**
     * Изменить язык интерфейса текущего пользователя
     *
     * @return void
     */
    public function changeLanguage() : void
    {
        Session::setParam('lang', $this->lang);
    }

    /**
     * @inheritDoc
     */
    public function getAttributes() : array
    {
        return ['lang'];
    }

    /**
     * @inheritDoc
     */
    public function getNormalizers() : array
    {
        return [
            'lang' => $this->validatorFactory->createStringNormalizer(),
        ];
    }

    /**
     * @inheritDoc
     */
    public function getValidators() : array
    {
        return [
            'lang' => $this->validatorFactory->createLangValidator(),
        ];
    }
}
