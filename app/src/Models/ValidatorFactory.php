<?php

namespace TRLogic\Models;

use TRLogic\Validators\CompareValidator;
use TRLogic\Validators\CompositeValidator;
use TRLogic\Validators\EmailNormalizer;
use TRLogic\Validators\EmailValidator;
use TRLogic\Validators\ExistsValidator;
use TRLogic\Validators\NormalizerInterface;
use TRLogic\Validators\RegexValidator;
use TRLogic\Validators\StringLengthValidator;
use TRLogic\Validators\StringNormalizer;
use TRLogic\Validators\UploadedFileValidator;
use TRLogic\Validators\ValidatorInterface;
use TRLogic\Web\Application;

/**
 * Class ValidatorFactory
 *
 * Фабрика валидаторов
 */
class ValidatorFactory
{
    /** @var int Минимальная длина почтового адреса */
    private const EMAIL_MIN_LENGTH = 6;

    /** @var int Максимальная длина почтового адреса */
    private const EMAIL_MAX_LENGTH = 255;

    /** @var string Шаблон регулярного выражения для проверки почтового адреса */
    private const EMAIL_RULE = '/^[A-Za-z0-9_.-]+@[A-Za-z0-9_.-]+$/';

    /** @var int Минимальная дли пароля */
    private const PASSWORD_MIN_LENGTH = 8;

    /** @var int Максимальная длина пароля */
    private const PASSWORD_MAX_LENGTH = 20;

    /** @var string Шаблон регулярного выражения для проверки пароля */
    private const PASSWORD_RULE = '/^[\x21-\x7e]+$/';

    /** @var int Минимальная длина полного имени пользователя */
    private const FULL_NAME_MIN_LENGTH = 2;

    /** @var int Максимальная длина полного имени пользователя */
    private const FULL_NAME_MAX_LENGTH = 100;

    /** @var string Шаблон регулярного выражения для проверки полного имени */
    private const FULL_NAME_RULE = '/^[^\x00-\x1f\x21-\x2c\x2f-\x40\x5b-\x60\x7b-\xff]+$/u';

    /** @var string[] Список разрешённых типов файла аватара */
    public const AVATAR_MIME_TYPES = [
        'image/jpeg' => 'jpg',
        'image/jpg' => 'jpg',
        'image/png' => 'png',
        'image/gif' => 'gif',
    ];

    /** @var int  Максимальный размер файла аватара */
    private const AVATAR_MAX_FILE_SIZE = 1048576;

    /**
     * @var string[] Коллекция языковых сообщений
     */
    private $messages;

    /**
     * ValidatorFactory constructor.
     */
    public function __construct()
    {
        $this->messages = Application::getInstance()->getI18n()->getMessages();
    }

    /**
     * Создать нормализатор email адреса
     *
     * @return NormalizerInterface
     */
    public function createEmailNormalizer() : NormalizerInterface
    {
        return new EmailNormalizer();
    }

    /**
     * Создать валидатор email адреса
     *
     * @return ValidatorInterface
     */
    public function createEmailValidator() : ValidatorInterface
    {
        return new CompositeValidator([
            new StringLengthValidator(
                $this->messages['ERROR_INVALID_EMAIL'],
                self::EMAIL_MIN_LENGTH,
                self::EMAIL_MAX_LENGTH
            ),
            new RegexValidator($this->messages['ERROR_INVALID_EMAIL'], self::EMAIL_RULE),
            new EmailValidator($this->messages['ERROR_INVALID_EMAIL']),
        ]);
    }

    /**
     * Создать валидатор пароля
     *
     * @return ValidatorInterface
     */
    public function createPasswordValidator() : ValidatorInterface
    {
        return new CompositeValidator([
            new StringLengthValidator(
                $this->messages['ERROR_PASSWORD_LENGTH'],
                self::PASSWORD_MIN_LENGTH,
                self::PASSWORD_MAX_LENGTH
            ),
            new RegexValidator($this->messages['ERROR_PASSWORD_FORMAT'], self::PASSWORD_RULE),
        ]);
    }

    /**
     * Создать валидатор сравнения значений
     *
     * @param mixed $value Эталонное значение
     * @return ValidatorInterface
     */
    public function createCompareValidator($value) : ValidatorInterface
    {
        return new CompareValidator($this->messages['ERROR_COMPARE_PASSWORDS'], 'password', $value);
    }

    /**
     * Создать нормализатор строки
     *
     * @return NormalizerInterface
     */
    public function createStringNormalizer() : NormalizerInterface
    {
        return new StringNormalizer();
    }

    /**
     * Создать валидатор полного имени пользователя
     *
     * @return ValidatorInterface
     */
    public function createFullNameValidator() : ValidatorInterface
    {
        return new CompositeValidator([
            new StringLengthValidator(
                $this->messages['ERROR_NAME_LENGTH'],
                self::FULL_NAME_MIN_LENGTH,
                self::FULL_NAME_MAX_LENGTH
            ),
            new RegexValidator($this->messages['ERROR_INVALID_NAME'], self::FULL_NAME_RULE),
        ]);
    }

    /**
     * Создать валидатор загруженного файла аватара
     *
     * @return ValidatorInterface
     */
    public function createAvatarValidator() : ValidatorInterface
    {
        return new UploadedFileValidator(
            $this->messages['ERROR_FILE_UPLOAD'],
            $this->messages['ERROR_FILE_TYPE'],
            $this->messages['ERROR_FILE_SIZE'],
            array_keys(self::AVATAR_MIME_TYPES),
            self::AVATAR_MAX_FILE_SIZE
        );
    }

    /**
     * Создать валидатор идентификатора языка
     *
     * @return ValidatorInterface
     */
    public function createLangValidator() : ValidatorInterface
    {
        $i18n = Application::getInstance()->getI18n();

        return new ExistsValidator($i18n->getMessages()['ERROR_INVALID_LANGUAGE'], $i18n->getLanguages());
    }
}
