<?php

namespace TRLogic\Web;

/**
 * Class View
 *
 * Выполняет сборку шаблонов
 */
class View
{
    /**
     * @var string[] Коллекция языковых сообщений
     */
    public $messages = [];

    /**
     * @var string[] Глобальная коллекция inline-скриптов
     */
    public $js = [];

    /**
     * @var string Глобальный заголовок страницы
     */
    public $title = '';

    /**
     * @var array[] Параметры собираемого шаблона
     */
    private $template;

    /**
     * @var string Базовая директория шаблонов
     */
    private $viewsDir;

    /**
     * View constructor.
     *
     * @param string $viewsDir Базовая директория шаблонов
     * @param array $messages Коллекция языковых сообщений
     */
    public function __construct(string $viewsDir, array $messages = [])
    {
        $this->viewsDir = $viewsDir;
        $this->messages = $messages;
    }

    /**
     * Собрать шаблон
     *
     * @param string $name Имя шаблона
     * @param array $params Коллекция параметров
     * @return string
     */
    public function renderTemplate(string $name, array $params = []) : string
    {
        $this->template = [
            'path' => sprintf('%s/%s.php', $this->viewsDir, $name),
            'params' => $params,
        ];
        ob_start();
        $this->requireTemplate();
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

    /**
     * Преобразовать специальные символы в HTML-сущности
     *
     * @param mixed $value
     * @return string
     */
    public static function encode($value) : string
    {
        return htmlspecialchars($value, ENT_COMPAT | ENT_HTML5, 'UTF-8');
    }

    /**
     * Передать параметры в шаблон и выполнить его как код PHP
     *
     * @return void
     */
    private function requireTemplate() : void
    {
        extract($this->template['params']);
        require $this->template['path'];
    }
}
