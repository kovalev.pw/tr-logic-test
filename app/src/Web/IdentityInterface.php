<?php

namespace TRLogic\Web;

/**
 * Interface IdentityInterface
 */
interface IdentityInterface
{
    /**
     * Получить идентификатор пользователя
     *
     * @return int
     */
    public function getId() : int;

    /**
     * Получить параметры авторизованного пользователя
     *
     * @return array Коллекция параметров пользователя
     */
    public function getData() : array;

    /**
     * Найти пользователя по его идентификатору
     *
     * @param int $id
     * @return static|null
     */
    public static function findById(int $id) : ?self;

    /**
     * Получить правильный хеш параметров пользователя
     *
     * @return string хеш
     */
    public function generateHash() : string;
}
