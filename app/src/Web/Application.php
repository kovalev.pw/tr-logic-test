<?php

namespace TRLogic\Web;

use ErrorException;
use PDO;

/**
 * Class Application
 *
 * Осуществляет инициализацию компонентов и запуск приложения.
 * Предоставляет глобальный доступ к общим компонентам.
 */
class Application
{
    /** @var string Имя папки публичных файлов */
    public const PUBLIC_DIR = 'public';
    /** @var string Имя папки ассетов */
    public const ASSETS_DIR = 'assets';
    /** @var string Имя папки загруженных временных файлов */
    public const UPLOADS_DIR = 'uploads';
    /** @var string Имя папки шаблонов */
    public const VIEWS_DIR = 'views';

    /**
     * @var mixed[] Параметры конфигурации приложения
     */
    private $config = [];

    /**
     * @var PDO Экземпляр БД
     */
    private $database;

    /**
     * @var I18n Экземпляр компонента интернационализации
     */
    private $i18n;

    /**
     * @var Request Экземпляр запроса
     */
    private $request;

    /**
     * @var Response Экземпляр ответа
     */
    private $response;

    /**
     * @var Auth Экземпляр пользователя
     */
    private $user;

    /**
     * @var static Экземпляр приложения
     */
    private static $instance;

    /**
     * Получить экземпляр приложения
     *
     * @return static
     */
    public static function getInstance() : self
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Запустить приложение
     *
     * @param array $config Параметры конфигурации
     * @return void
     */
    public static function run(array $config) : void
    {
        $app = self::getInstance();
        $app->config = $config;
        $app->initDatabase();
        $app->initUser();
        $app->initI18n();
        $app->handleRequest();
        $app->response->sendHeaders();
        $app->response->sendContent();
        $app::shutdownHandler();
    }

    /**
     * Получить базовую папку приложения
     *
     * @return string
     */
    public function getBaseDir() : string
    {
        return $this->config['baseDir'];
    }

    /**
     * Получить экземпляр текущей БД
     *
     * @return PDO
     */
    public function getDatabase() : PDO
    {
        return $this->database;
    }

    /**
     * Получить экземпляр компонента интернационализации
     *
     * @return I18n
     */
    public function getI18n() : I18n
    {
        return $this->i18n;
    }

    /**
     * Получить экземпляр текущего запроса
     *
     * @return Request
     */
    public function getRequest() : Request
    {
        return $this->request;
    }

    /**
     * Получить экземпляр авторизации пользователя
     *
     * @return Auth
     */
    public function getAuth() : Auth
    {
        return $this->user;
    }

    /**
     * Обработчик ошибок
     *
     * @param int $errno Номер ошибки
     * @param string $error Описание ошибки
     * @param string $file Имя файла ошибки
     * @param int $line Линия файла ошибки
     * @return bool
     * @throws ErrorException
     */
    public static function errorHandler(int $errno, string $error, string $file, int $line) : bool
    {
        if (error_reporting() & $errno) {
            throw new ErrorException($error, $errno, 0, $file, $line);
        }

        return false;
    }

    /**
     * Обработчик завершения скрипта
     *
     * @return void
     */
    public static function shutdownHandler() : void
    {
        restore_error_handler();

        while (ob_get_level()) {
            ob_end_flush();
        }
    }

    /**
     * Application constructor.
     */
    private function __construct()
    {
        $this->request = new Request();
        $this->response = new Response();

        register_shutdown_function([self::class, 'shutdownHandler']);
        set_error_handler([self::class, 'errorHandler']);
    }

    /**
     * Инициализировать БД
     *
     * @return void
     */
    private function initDatabase() : void
    {
        $cfg = $this->config['database'];
        $this->database = new PDO($cfg['dsn'], $cfg['username'], $cfg['password']);
        $this->database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Инициализировать компонент интернационализации
     *
     * @return void
     */
    private function initI18n() : void
    {
        $currentLang = Session::getParam('lang', $this->config['i18n']['lang']);
        $this->i18n = new I18n($this->config['i18n']['messages'], $currentLang);
    }

    /**
     * Инициализировать пользователя
     *
     * @return void
     */
    private function initUser() : void
    {
        Session::init($this->config['session']);

        $this->user = new Auth($this->config['identity']['class']);
    }

    /**
     * Обработать запрос
     *
     * @return void
     */
    private function handleRequest() : void
    {
        try {
            $action = $this->request->getCurrentAction();
            $controller = new Controller();
            $method = 'action' . $action;

            if (parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) != '/' || !method_exists($controller, $method)) {
                throw new NotFoundException();
            }

            $this->response->content = $controller->$method();
            $this->response->contentType = Response::TEXT_HTML_TYPE;
        } catch (HttpExceptionInterface $exception) {
            $this->response->code = $exception->getCode();
            $this->response->content = $exception->getMessage();

            if ($exception instanceof RedirectException) {
                $this->response->location = $exception->getLocation();
            }
        }
    }
}
