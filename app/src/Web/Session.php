<?php

namespace TRLogic\Web;

/**
 * Class Session
 *
 * Управляет текущей сессией пользователя
 */
class Session
{
    /**
     * Инициализировать механизм сессий
     *
     * @param string[] $options Коллекция опций сессии
     */
    public static function init(array $options)
    {
        session_start($options);
    }

    /**
     * @return void
     */
    public static function regenerateId() : void
    {
        session_regenerate_id();
    }

    /**
     * Получить параметр сессии
     *
     * @param string $name Имя параметра
     * @param mixed $defaultValue Возвращаемое значение по умолчанию
     * @return mixed Значение параметра
     */
    public static function getParam(string $name, $defaultValue = null)
    {
        return $_SESSION[$name] ?? $defaultValue;
    }

    /**
     * Установить параметр сессии
     *
     * @param string $name Имя параметра
     * @param mixed $value Значение параметра
     * @return void
     */
    public static function setParam(string $name, $value) : void
    {
        $_SESSION[$name] = $value;

        if ($_SESSION[$name] === null) {
            unset($_SESSION[$name]);
        }
    }

    /**
     * Получить значение параметра сессии и удалить его из сессии
     *
     * @param string $name Имя параметра
     * @return mixed Значение параметра
     */
    public static function flashParam(string $name)
    {
        $value = self::getParam($name);

        if ($value !== null) {
            self::setParam($name, null);
        }

        return $value;
    }

    /**
     * Session constructor.
     */
    final private function __construct()
    {
    }
}
