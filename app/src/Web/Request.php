<?php

namespace TRLogic\Web;

/**
 * Class Request
 *
 * Обеспечивает доступ к параметрам текущего HTTP-запроса
 */
class Request
{
    /**
     * @var bool Определяет, является ли текущий запрос типом POST
     */
    private $isPost = false;

    /**
     * @var string[] Список POST параметров
     */
    private $postData = [];

    /**
     * @var string[] Список GET параметров
     */
    private $queryData = [];

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->isPost = (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST');
        $this->postData = array_filter($_POST, 'is_scalar');
        $this->queryData = array_filter($_GET, 'is_scalar');
    }

    /**
     * Получить идентификатор текущего действия
     *
     * @return string Идентификатор текущего действия
     */
    public function getCurrentAction() : string
    {
        return $this->queryData['action'] ?? 'default';
    }

    /**
     * Определить, является ли текущий запрос типом POST
     *
     * @return bool
     */
    public function isPost() : bool
    {
        return $this->isPost;
    }

    /**
     * Получить список POST параметров запроса
     *
     * @return string[] Коллекция параметров
     */
    public function getPostData() : array
    {
        return $this->postData;
    }

    /**
     * Получить список GET параметров запроса
     *
     * @return string[] Коллекция параметров
     */
    public function getQueryData() : array
    {
        return $this->queryData;
    }
}
