<?php

namespace TRLogic\Web;

/**
 * Class Response
 *
 * Формирует текущий HTTP-ответ
 */
class Response
{
    public const TEXT_HTML_TYPE = 'text/html; charset=utf-8';
    public const TEXT_PLAIN_TYPE = 'text/plain; charset=utf-8';

    /**
     * @var int Код ответа
     */
    public $code = 200;

    /**
     * @var string|null Значение HTTP-заголовка Location
     */
    public $location;

    /**
     * @var string Содержимое тела HTTP-ответа
     */
    public $content = '';

    /**
     * @var string Значение HTTP-заголовка Content-Type
     */
    public $contentType = self::TEXT_PLAIN_TYPE;

    /**
     * Отправляет клиенту тело HTTP-ответа
     *
     * @return void
     */
    public function sendContent() : void
    {
        echo $this->content;
    }

    /**
     * Отправляет клиенту HTTP-заголовки
     *
     * @return void
     */
    public function sendHeaders() : void
    {
        if (!headers_sent()) {
            http_response_code($this->code);
            header("Content-Type: {$this->contentType}");

            if ($this->location) {
                header("Location: {$this->location}");
                $this->content = '';
            }
        }
    }
}
