<?php

namespace TRLogic\Web;

use Error;

/**
 * Class ContentDecorator
 *
 * Помещает содержимое, находящиеся между begin и end, внутрь другого шаблона
 */
class ContentDecorator
{
    /**
     * @var static Текущий экземпляр класса
     */
    private static $instance;

    /**
     * @var View Экземпляр текущего представления
     */
    private $view;

    /**
     * @var string Имя шаблона
     */
    private $templateName;

    /**
     * @var mixed[] Параметры шаблона
     */
    private $params = [];

    /**
     * Начать буферизацию содержимого объявленного ниже
     *
     * @param View $view Экземпляр текущего представления
     * @param string $templateName Имя шаблона
     * @param mixed[] $params Параметры шаблона
     * @return void
     */
    public static function begin(View $view, string $templateName, array $params = []) : void
    {
        if (self::$instance !== null) {
            self::$instance = null;
            throw new Error(sprintf('%s already taken', self::class), E_USER_ERROR);
        }

        self::$instance = new self($view, $templateName, $params);
    }

    /**
     * Закончить буферизацию содержимого объявленного выше
     *
     * @return void
     */
    public static function end() : void
    {
        if (self::$instance === null) {
            throw new Error(sprintf('%s instance undefined', self::class), E_USER_ERROR);
        }

        self::$instance = null; // Вызывает деструктор класса
    }

    /**
     * Собрать шаблон и отправить его содержимое в поток
     *
     * @return void
     */
    public function __destruct()
    {
        $this->params['content'] = ob_get_contents();
        ob_end_clean();

        echo $this->view->renderTemplate($this->templateName, $this->params);
    }

    /**
     * ContentDecorator constructor.
     *
     * @param View $view Экземпляр текущего представления
     * @param string $templateName Имя шаблона
     * @param mixed[] $params Параметры шаблона
     */
    private function __construct(View $view, string $templateName, array $params)
    {
        $this->view = $view;
        $this->templateName = $templateName;
        $this->params = $params;
        ob_start();
    }
}
