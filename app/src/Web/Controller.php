<?php

namespace TRLogic\Web;

use TRLogic\Models\LanguageForm;
use TRLogic\Models\LoginForm;
use TRLogic\Models\RegistrationForm;
use TRLogic\Validators\ModelVerifier;

/**
 * Class Controller
 */
class Controller
{
    /**
     * @var Request Текущий экземпляр запроса
     */
    private $request;

    /**
     * @var Auth Текущий экземпляр пользователя
     */
    private $auth;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->request = Application::getInstance()->getRequest();
        $this->auth = Application::getInstance()->getAuth();
    }

    /**
     * Действие по умолчанию (index)
     *
     * @return void
     * @throws RedirectException Всегда на страницу пользователя
     */
    public function actionDefault() : void
    {
        throw new RedirectException('/?action=profile');
    }

    /**
     * Выполнить авторизацию пользователя
     *
     * @return string
     * @throws RedirectException Если пользователь был авторизован
     */
    public function actionLogin() : string
    {
        if (!$this->auth->isGuest()) {
            throw new RedirectException('/?action=profile');
        }

        $form = new LoginForm();

        if ($this->request->isPost()) {
            $verifier = new ModelVerifier($form);
            $verifier->loadData($this->request->getPostData());

            if ($verifier->validateModel() && $form->login()) {
                throw new RedirectException('/?action=profile');
            }
        }

        return $this->renderView('actions/login', compact('form'));
    }

    /**
     * Сменить язык интерфейса текущего пользователя
     *
     * @return void
     * @throws NotFoundException Если был передан неизвестный идентификатор языка
     * @throws RedirectException Всегда на указанное действие
     */
    public function actionLanguage() : void
    {
        $queryData = $this->request->getQueryData();
        $form = new LanguageForm();
        $verifier = new ModelVerifier($form);
        $verifier->loadData($queryData);

        if ($verifier->validateModel()) {
            $form->changeLanguage();

            throw new RedirectException('/?action=' . $queryData['return'] ?? 'profile');
        }

        throw new NotFoundException($form->getErrors()->getItem('lang'));
    }

    /**
     * Завершить сессию текущего пользователя
     *
     * @return void
     * @throws RedirectException Всегда на страницу авторизации
     */
    public function actionLogout() : void
    {
        $this->auth->signOut();

        throw new RedirectException('/?action=login');
    }

    /**
     * Показать профиль текущего пользователя
     *
     * @return string
     * @throws RedirectException Если пользователь не авторизован
     */
    public function actionProfile() : string
    {
        if ($this->auth->isGuest()) {
            throw new RedirectException('/?action=login');
        }

        $user = $this->auth->getIdentity()->getData();

        return $this->renderView('actions/profile', compact('user'));
    }

    /**
     * Выполнить регистрацию пользователя
     *
     * @return string
     * @throws ForbiddenException Если пользователь авторизован
     * @throws RedirectException Если регистрация выполнена
     */
    public function actionRegistration() : string
    {
        if (!$this->auth->isGuest()) {
            throw new ForbiddenException();
        }

        $form = new RegistrationForm();

        if ($this->request->isPost()) {
            $verifier = new ModelVerifier($form);
            $verifier->loadData($this->request->getPostData());

            if ($verifier->validateModel() && $form->registerUser()) {
                Session::setParam('registered', true);

                throw new RedirectException('/?action=login');
            }
        }

        return $this->renderView('actions/registration', compact('form'));
    }

    /**
     * Собрать представление
     *
     * @param string $viewName Имя шаблона
     * @param array $params Список параметров
     * @return string
     */
    private function renderView(string $viewName, array $params) : string
    {
        $messages = Application::getInstance()->getI18n()->getMessages();
        $viewDir = Application::getInstance()->getBaseDir() . '/' . Application::VIEWS_DIR;
        $view = new View($viewDir, $messages);
        $content = $view->renderTemplate($viewName, $params);

        return $view->renderTemplate('layouts/default', compact('content'));
    }
}
