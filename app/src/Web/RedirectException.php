<?php

namespace TRLogic\Web;

use Exception;

/**
 * Class RedirectException
 *
 * Исключение HTTP-ответа 302 Found
 */
class RedirectException extends Exception implements HttpExceptionInterface
{
    /** @var int Код ответа */
    private const HTTP_CODE = 302;

    /**
     * @var string Значение HTTP-заголовка Location
     */
    private $location;

    /**
     * RedirectException constructor.
     *
     * @param string $location Значение HTTP-заголовка Location
     */
    public function __construct(string $location)
    {
        $this->location = $location;

        parent::__construct('', self::HTTP_CODE);
    }

    /**
     * @inheritDoc
     */
    public function getLocation() : string
    {
        return $this->location;
    }
}
