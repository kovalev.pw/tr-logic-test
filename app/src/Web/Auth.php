<?php

namespace TRLogic\Web;

use Error;

/**
 * Class Auth
 *
 * Управляет авторизацией пользователя
 */
class Auth
{
    /**
     * @var IdentityInterface|null Экземпляр текущего пользователя
     */
    private $identity;

    /**
     * Auth constructor.
     *
     * @param string $identityClass
     */
    public function __construct(string $identityClass)
    {
        $params = Session::getParam('user');

        if ($params) {
            $identity = $this->restoreIdentity($identityClass, $params);

            if ($identity) {
                $this->signIn($identity);
            } else {
                $this->signOut();
            }
        }
    }

    /**
     * Восстановить пользователя по параметрам сессии
     *
     * @param string $identityClass Имя класса пользователя
     * @param mixed $params Параметры сессии
     * @return IdentityInterface|null
     */
    private function restoreIdentity(string $identityClass, $params) : ?IdentityInterface
    {
        if (!is_array($params) || !array_filter($params, 'is_scalar')) {
            return null;
        }

        if (!isset($params['id'], $params['hash'], $params['validated_at'])) {
            return null;
        }

        if (self::isExpired($params['validated_at'])) {
            return null;
        }

        /** @var IdentityInterface $identityClass */
        $identity = $identityClass::findById($params['id']);

        if (!$identity || $params['hash'] != self::getValidHash($identity)) {
            return null;
        }

        return $identity;
    }

    /**
     * Определить, является ли текущий пользователь гостем
     *
     * @return bool Вернуть TRUE если пользователь не авторизован, иначе FALSE
     */
    public function isGuest() : bool
    {
        return $this->identity ? false : true;
    }

    /**
     * Получить экземпляр текущего пользователя
     *
     * @return IdentityInterface|null
     */
    public function getIdentity() : ?IdentityInterface
    {
        return $this->identity;
    }

    /**
     * Получить правильный хеш параметров сессии
     *
     * @param IdentityInterface $identity
     * @return string хеш
     */
    public static function getValidHash(IdentityInterface $identity) : string
    {
        $values = [$identity->generateHash(), $_SERVER['HTTP_USER_AGENT'] ?? ''];

        return md5(implode(':', $values));
    }

    /**
     * Авторизовать пользователя
     *
     * @param IdentityInterface $identity Экземпляр авторизованного пользователя
     * @return void
     */
    public function signIn(IdentityInterface $identity) : void
    {
        $this->identity = $identity;

        Session::setParam('user', [
            'id' => $identity->getId(),
            'hash' => self::getValidHash($identity),
            'validated_at' => gmdate('U'),
        ]);
    }

    /**
     * Закончить сессию пользователя
     *
     * @return void
     */
    public function signOut() : void
    {
        $this->identity = null;

        Session::setParam('user', null);
        Session::regenerateId();
    }

    /**
     * Определить, просрочена ли сессия
     *
     * @param int $validatedAt Время последней валидации
     * @return bool Вернуть TRUE если время сессии истекло, иначе FALSE
     */
    private static function isExpired(int $validatedAt) : bool
    {
        $lifetime = session_get_cookie_params()['lifetime'];

        if ($lifetime < 1) {
            throw new Error('Value "session.cookie_lifetime" is invalid', E_USER_ERROR);
        }

        if ((gmdate('U') - $validatedAt) < $lifetime) {
            return false;
        }

        return true;
    }
}
