<?php

namespace TRLogic\Web;

use Exception;

/**
 * Class ForbiddenException
 *
 * Исключение HTTP-ответа 403 Forbidden
 */
class ForbiddenException extends Exception implements HttpExceptionInterface
{
    /** @var int Код ответа */
    private const HTTP_CODE = 403;
    /** @var string Сообщение пользователю по умолчанию */
    private const DEFAULT_MESSAGE = '403 Forbidden';

    /**
     * NotFoundException constructor.
     *
     * @param string $message Сообщение пользователю
     */
    public function __construct(string $message = self::DEFAULT_MESSAGE)
    {
        parent::__construct($message, self::HTTP_CODE);
    }
}
