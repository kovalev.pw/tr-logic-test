<?php

namespace TRLogic\Web;

/**
 * Class I18n
 *
 * Осуществляет выбор списка языковых сообщений
 */
class I18n
{
    /**
     * @var string Идентификатор текущего языка
     */
    private $currentLang;

    /**
     * @var string[] Коллекция сообщений всех доступных языков
     */
    private $messages = [];

    /**
     * I18n constructor.
     *
     * @param string[][] $messages Коллекция сообщений всех доступных языков
     * @param string $currentLang Идентификатор текущего языка
     */
    public function __construct(array $messages, string $currentLang)
    {
        $this->messages = $messages;
        $this->currentLang = $currentLang;
    }

    /**
     * Получить идентификатор текущего языка
     *
     * @return string
     */
    public function getCurrentLang() : string
    {
        return $this->currentLang;
    }

    /**
     * Получить список доступных языков
     *
     * @return string[] Список языков
     */
    public function getLanguages() : array
    {
        return array_keys($this->messages);
    }

    /**
     * Получить список языковых сообщений
     *
     * @param string|null $lang Идентификатор языка
     * @return string[] Коллекция сообщений
     */
    public function getMessages(string $lang = null) : array
    {
        return $this->messages[$lang ?: $this->currentLang];
    }
}
