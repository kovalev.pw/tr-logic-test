<?php

namespace TRLogic\Web;

use Exception;

/**
 * Class NotFoundException
 *
 * Исключение HTTP-ответа 404 Forbidden
 */
class NotFoundException extends Exception implements HttpExceptionInterface
{
    /** @var int Код ответа */
    private const HTTP_CODE = 404;
    /** @var string Сообщение пользователю по умолчанию */
    private const DEFAULT_MESSAGE = '404 Not Found';

    /**
     * NotFoundException constructor.
     *
     * @param string $message Сообщение пользователю
     */
    public function __construct(string $message = self::DEFAULT_MESSAGE)
    {
        parent::__construct($message, self::HTTP_CODE);
    }
}
