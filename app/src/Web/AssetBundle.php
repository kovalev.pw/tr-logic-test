<?php

namespace TRLogic\Web;

/**
 * Class AssetBundle
 *
 * Опубликовывает asset файлы
 */
class AssetBundle
{
    /** @var string Имя папки css-стилей */
    private const CSS_DIR = 'css';

    /** @var string Имя папки js-скриптов */
    private const JS_DIR = 'js';

    /**
     * @var string Полный путь исходного файла
     */
    private $sourcePath;

    /**
     * @var string Полный путь копии файла
     */
    private $destinationPath;

    /**
     * @var string Публичная ссылка
     */
    private $publicLink;

    /**
     * Создать экземпляр для css-стиля
     *
     * @param string $sourcePath Относительный путь исходного файла
     * @return static
     */
    public static function createStyle(string $sourcePath) : self
    {
        return new self($sourcePath, self::CSS_DIR);
    }

    /**
     * Создать экземпляр для js-скрипта
     *
     * @param string $sourcePath Относительный путь исходного файла
     * @return static
     */
    public static function createScript(string $sourcePath) : self
    {
        return new self($sourcePath, self::JS_DIR);
    }

    /**
     * AssetBundle constructor.
     *
     * @param string $sourcePath Относительный путь исходного файла
     * @param string $dir Имя папки назначения
     */
    private function __construct(string $sourcePath, string $dir)
    {
        $baseDir = Application::getInstance()->getBaseDir();
        $this->sourcePath = $this->buildPath([$baseDir, $sourcePath]);
        $baseName = basename($sourcePath);
        $pieces = [$baseDir, Application::PUBLIC_DIR, Application::ASSETS_DIR, $dir, $baseName];
        $this->destinationPath = $this->buildPath($pieces);
        $this->publicLink = $this->buildPath(['', Application::ASSETS_DIR, $dir, $baseName]);
    }

    /**
     * Собрать путь до файла
     *
     * @param string[] $pieces Список частей
     * @return string
     */
    private function buildPath(array $pieces) : string
    {
        return str_replace('//', '/', implode(DIRECTORY_SEPARATOR, $pieces));
    }

    /**
     * Получить публичную ссылку
     *
     * @return string
     */
    public function getLink()
    {
        $this->publishAsset();
        $modTime = dechex(filemtime($this->destinationPath));

        return implode('?v=', [$this->publicLink, $modTime]);
    }

    /**
     * Опубликовать файл
     *
     * @return void
     */
    private function publishAsset() : void
    {
        if ($this->sourcePath == $this->destinationPath) {
            return;
        }

        if (!file_exists($this->destinationPath) || filesize($this->destinationPath) != filesize($this->sourcePath)) {
            copy($this->sourcePath, $this->destinationPath);
        }

        return;
    }
}
