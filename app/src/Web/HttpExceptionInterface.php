<?php

namespace TRLogic\Web;

use Throwable;

/**
 * Interface HttpExceptionInterface
 *
 * Интерфейс исключений HTTP-ответа
 */
interface HttpExceptionInterface extends Throwable
{

}
