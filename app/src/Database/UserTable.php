<?php

namespace TRLogic\Database;

use TRLogic\Web\Application;
use PDO;

/**
 * Class UserTable
 *
 * Осуществляет манипуляции с записями таблицы users
 */
class UserTable
{
    /**
     * Найти запись по условиям
     *
     * @param string[] $condition Коллекция условий
     * @return string[] Коллекция полей
     */
    public static function findRow(array $condition) : array
    {
        $where = self::buildWhere($condition);
        $params = self::buildParams($condition);
        $statement = Application::getInstance()
            ->getDatabase()
            ->prepare("SELECT * FROM users WHERE {$where}");
        $statement->execute($params);

        return $statement->fetch(PDO::FETCH_ASSOC) ?: [];
    }

    /**
     * Вставить новую запись в таблицу
     *
     * @param string[] $row Коллекция полей
     * @return bool Вернуть TRUE в случае успеха, иначе FALSE
     */
    public static function insertRow(array $row) : bool
    {
        $params = self::buildParams($row);
        $columns = implode(', ', array_keys($row));
        $values = implode(', ', array_keys($params));
        $statement = Application::getInstance()
            ->getDatabase()
            ->prepare("INSERT INTO users ({$columns}) VALUES ({$values})");

        return $statement->execute($params);
    }

    /**
     * Собрать параметры запроса
     *
     * @param string[] $data Коллекция полей
     * @return string[] Коллекция параметров
     * @see PDOStatement::execute()
     */
    private static function buildParams(array $data) : array
    {
        $params = [];

        array_walk($data, function ($value, string $column) use (&$params) {
            $params[":{$column}"] = $value;
        });

        return $params;
    }

    /**
     * Собрать параметры оператора WHERE
     *
     * @param string[] $condition Коллекция условий
     * @return string SQL-код оператора WHERE
     */
    private static function buildWhere(array $condition) : string
    {
        $keys = array_keys($condition);
        $where = array_map(function (string $key) {
            return "{$key} = :{$key}";
        }, $keys);

        return implode(' AND ', $where);
    }
}
