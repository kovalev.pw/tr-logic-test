<?php

namespace TRLogic\Collections;

use IteratorAggregate;

/**
 * Interface CollectionInterface
 *
 * Интерфейс коллекции
 */
interface CollectionInterface extends IteratorAggregate
{
    /**
     * Получить значение
     *
     * @param mixed $key Ключ значения
     * @param mixed $defaultValue Значение по умолчанию
     * @return mixed Значение
     */
    public function getItem($key, $defaultValue = null);

    /**
     * Установить значение
     *
     * @param mixed $key Ключ значения
     * @param mixed $value Значение
     * @return void
     */
    public function setItem($key, $value) : void;

    /**
     * Получить массив всех значений
     *
     * @return mixed[]
     */
    public function getItems() : array;

    /**
     * Определить существование значения к коллекции
     *
     * @param mixed $key Ключ значения
     * @return bool Вернуть TRUE если ключ существует, иначе FALSE
     */
    public function hasItem($key) : bool;

    /**
     * Определить, является ли коллекция пустой
     *
     * @return bool Вернуть TRUE если коллекция пуста, иначе FALSE
     */
    public function isEmpty() : bool;

    /**
     * Удалить значение
     *
     * @param mixed $key Ключ значения
     * @return void
     */
    public function removeItem($key) : void;
}
