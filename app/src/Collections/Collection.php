<?php

namespace TRLogic\Collections;

use ArrayIterator;
use ArrayObject;

/**
 * Class Collection
 *
 * Коллекция ключей и значений
 */
class Collection implements CollectionInterface
{
    /**
     * @var ArrayObject
     */
    private $items;

    /**
     * Collection constructor.
     *
     * @param mixed[] $items
     */
    public function __construct(array $items = [])
    {
        $this->items = new ArrayObject($items);
    }

    /**
     * @inheritDoc
     */
    public function getItem($key, $defaultValue = null)
    {
        if ($this->items->offsetExists($key)) {
            return $this->items->offsetGet($key);
        }

        return $defaultValue;
    }

    /**
     * @inheritDoc
     */
    public function getItems() : array
    {
        return $this->items->getArrayCopy();
    }

    /**
     * @inheritDoc
     */
    public function getIterator()
    {
        return new ArrayIterator($this->getItems());
    }

    /**
     * @inheritDoc
     */
    public function hasItem($key) : bool
    {
        return $this->items->offsetExists($key);
    }

    /**
     * @inheritDoc
     */
    public function isEmpty() : bool
    {
        return ($this->items->count() == 0);
    }

    /**
     * @inheritDoc
     */
    public function setItem($key, $value) : void
    {
        $this->items->offsetSet($key, $value);
    }

    /**
     * @inheritDoc
     */
    public function removeItem($key) : void
    {
        if ($this->items->offsetExists($key)) {
            $this->items->offsetUnset($key);
        }
    }
}
