<?php

use TRLogic\Web\Application;
use TRLogic\Web\View;

/**
 * Шаблон поля загружаемого файла
 *
 * @var View $this
 * @var string $formId Идентификатор формы
 * @var string $name Значение имени поля
 * @var string $label Значение метки поля
 * @var string $placeholder Значение заполнителя поля
 * @var string $value Значение поля
 * @var string $error Описание ошибки для поля
 */

$lang = Application::getInstance()->getI18n()->getCurrentLang();
$id = implode('-', [$formId, $name]);

?>
<div class="form-group">
    <label for="<?= $id ?>"><?= View::encode($label) ?></label>
    <div class="input-group">
        <div class="custom-file">
            <input type="file" name="avatar" class="custom-file-input" id="<?= $id ?>" autocomplete="off">
            <label
                class="custom-file-label text-truncate"
                for="<?= $id ?>"
                data-content="<?= View::encode($placeholder) ?>"
                lang="<?= $lang ?>">
                <?= View::encode($placeholder) ?>
            </label>
        </div>
        <div class="input-group-append">
            <button class="btn btn-secondary" type="button" onclick="Application.Form.resetInput('<?= $id ?>');">
                <strong>&times;</strong>
            </button>
        </div>
    </div>
    <div class="invalid-feedback" id="<?= $id ?>-error"><?= View::encode($error) ?></div>
</div>
