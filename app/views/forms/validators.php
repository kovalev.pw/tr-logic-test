<?php

use TRLogic\Validators\FrontendAdapter;
use TRLogic\Validators\ModelInterface;
use TRLogic\Web\View;

/**
 * @var View $this
 * @var string $formId Идентификатор формы
 * @var ModelInterface $form Модель формы
 */

$adapter = new FrontendAdapter($form);
$args = implode(',', ["'{$formId}'", json_encode($adapter->getValidatorsData())]);

?>
<script>
(function () {
    Application.Form.hookSubmit(<?= $args ?>);
})();
</script>
