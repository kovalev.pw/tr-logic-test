<?php

use TRLogic\Web\View;

/**
 * Шаблон поля формы
 *
 * @var View $this
 * @var string $formId Идентификатор формы
 * @var string $name Значение имени поля
 * @var string $type Тип поля
 * @var string $label Значение метки поля
 * @var string $placeholder Значение заполнителя поля
 * @var string $value Значение поля
 * @var string $error Описание ошибки для поля
 */

$id = implode('-', [$formId, $name]);

?>
<div class="form-group">
    <label for="<?= $id ?>"><?= View::encode($label) ?></label>
    <input
        id="<?= $id ?>"
        type="<?= $type ?>"
        class="form-control"
        name="<?= $name ?>"
        placeholder="<?= $placeholder ?>"
        value="<?= View::encode($value) ?>"
        autocomplete="off">
    <div class="invalid-feedback" id="<?= $id ?>-error"><?= View::encode($error) ?></div>
</div>
