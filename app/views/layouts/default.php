<?php

use TRLogic\Web\Application;
use TRLogic\Web\AssetBundle;
use TRLogic\Web\View;

/**
 * Шаблон основного макета страницы
 *
 * @var View $this
 * @var string $content Оборачиваемое содержимое
 */

$bootstrapAsset = AssetBundle::createStyle('vendor/twbs/bootstrap/dist/css/bootstrap.min.css');
$styleAsset = AssetBundle::createStyle('public/assets/css/style.css');
$applicationAsset = AssetBundle::createScript('public/assets/js/application.js');

?>
<!doctype html>
<html lang="<?= Application::getInstance()->getI18n()->getCurrentLang(); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="stylesheet" href="<?= $bootstrapAsset->getLink() ?>">
    <link rel="stylesheet" href="<?= $styleAsset->getLink() ?>">
    <title><?= View::encode($this->title) ?></title>
</head>
<body>
<?= $content ?>
<script src="<?= $applicationAsset->getLink() ?>"></script>
<?php array_walk($this->js, function ($js) {
    echo $js;
}) ?>
</body>
</html>
