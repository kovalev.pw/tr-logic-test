<?php

use TRLogic\Web\View;

/**
 * Шаблон модального окна
 *
 * @var View $this
 * @var string $content Оборачиваемое содержимое
 * @var string $title Заголовок окна
 * @var string $header Дополнительное содержимое заголовка
 */

?>
<div class="container-fluid bg-dark">
<div class="row">
<div class="col-md-12">
<div class="modal d-block position-static">
<div class="modal-dialog modal-dialog-centered center">
<div class="modal-content">
<div class="modal-header">
    <h5 class="modal-title"><?= View::encode($title) ?></h5>
    <?= $header ?>
</div>
<div class="modal-body">
<?= $content ?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
