<?php

use TRLogic\Web\ContentDecorator;
use TRLogic\Web\View;

/**
 * Шаблон страницы профиля
 *
 * @var View $this
 * @var string[] $user Данные текущего пользователя
 */

$formId = 'registration';
$this->title = $user['full_name'];
$avatar = $user['avatar'] ? "/assets/avatars/{$user['avatar']}" : '/android-chrome-512x512.png';

?>
<?php ContentDecorator::begin($this, 'decorators/modal', [
    'title' => $this->messages['ACTION_PROFILE'],
    'header' => '<a href="/?action=logout" class="close">&times;</a>',
]) ?>
<div class="row">
    <div class="col-12 text-break"><h5><?= View::encode($user['full_name']) ?></h5></div>
    <div class="col-9 mb-4 mx-auto">
        <img class="img-fluid rounded" src="<?= $avatar ?>" alt="<?= View::encode($user['full_name']) ?>">
    </div>
    <div class="col-12">
        <div><?= $this->messages['LABEL_EMAIL'] ?>:</div>
        <div class="text-break"><strong><?= View::encode($user['email']) ?></strong></div>
        <div><?= $this->messages['LABEL_CREATED_AT'] ?>:</div>
        <div><strong><?= $user['created_at'] ?> UTC</strong></div>
    </div>
</div>
<?php ContentDecorator::end() ?>
