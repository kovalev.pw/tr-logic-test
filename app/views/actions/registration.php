<?php

use TRLogic\Models\RegistrationForm;
use TRLogic\Web\ContentDecorator;
use TRLogic\Web\View;

/**
 * Шаблон страницы регистрации
 *
 * @var View $this
 * @var RegistrationForm $form Форма регистрации пользователя
 */

$formId = 'registration';
$this->title = $this->messages['ACTION_REGISTRATION'];
$this->js[$formId] = $this->renderTemplate('forms/validators', compact('form', 'formId'));

?>
<?php ContentDecorator::begin($this, 'decorators/modal', [
    'title' => $this->messages['MODAL_REGISTRATION'],
    'header' => $this->renderTemplate('misc/language', ['action' => 'registration']),
]) ?>
<form action="/?action=registration" method="post" enctype="multipart/form-data" id="<?= $formId ?>">
    <?= $this->renderTemplate('forms/field-text', [
        'formId' => $formId,
        'name' => 'full_name',
        'type' => 'text',
        'label' => $this->messages['LABEL_FULL_NAME'],
        'placeholder' => $this->messages['PLACEHOLDER_FULL_NAME'],
        'value' => $form->full_name,
        'error' => $form->getErrors()->getItem('full_name'),
    ]) ?>
    <?= $this->renderTemplate('forms/field-text', [
        'formId' => $formId,
        'name' => 'email',
        'type' => 'email',
        'label' => $this->messages['LABEL_EMAIL'],
        'placeholder' => $this->messages['PLACEHOLDER_EMAIL'],
        'value' => $form->email,
        'error' => $form->getErrors()->getItem('email'),
    ]) ?>
    <?= $this->renderTemplate('forms/field-text', [
        'formId' => $formId,
        'name' => 'password',
        'type' => 'password',
        'label' => $this->messages['LABEL_PASSWORD'],
        'placeholder' => $this->messages['PLACEHOLDER_PASSWORD'],
        'value' => '',
        'error' => $form->getErrors()->getItem('password'),
    ]) ?>
    <?= $this->renderTemplate('forms/field-text', [
        'formId' => $formId,
        'name' => 'password_repeat',
        'type' => 'password',
        'label' => $this->messages['LABEL_PASSWORD_REPEAT'],
        'placeholder' => $this->messages['PLACEHOLDER_PASSWORD_REPEAT'],
        'value' => '',
        'error' => $form->getErrors()->getItem('password_repeat'),
    ]) ?>
    <?= $this->renderTemplate('forms/field-file', [
        'formId' => $formId,
        'name' => 'avatar',
        'label' => $this->messages['LABEL_AVATAR'],
        'placeholder' => $this->messages['PLACEHOLDER_AVATAR'],
        'error' => $form->getErrors()->getItem('password_repeat'),
    ]) ?>
    <div class="form-group">
        <button type="submit" class="btn btn-primary"><?= $this->messages['BUTTON_REGISTRATION'] ?></button>
        <a class="btn btn-link btn-sm" href="/?action=login"><?= $this->messages['BUTTON_CANCEL'] ?></a>
    </div>
</form>
<?php ContentDecorator::end() ?>
