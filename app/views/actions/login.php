<?php

use TRLogic\Models\LoginForm;
use TRLogic\Web\ContentDecorator;
use TRLogic\Web\Session;
use TRLogic\Web\View;

/**
 * Шаблон страницы авторизации
 *
 * @var View $this
 * @var LoginForm $form Форма авторизации пользователя
 */

$formId = 'login';
$this->title = $this->messages['ACTION_LOGIN'];
$this->js[$formId] = $this->renderTemplate('forms/validators', compact('form', 'formId'));

?>
<?php if (Session::flashParam('registered')): ?>
<div class="container fixed-top" id="alert">
    <div class="alert alert-success alert-dismissible mt-1">
        <div><?= $this->messages['MESSAGE_REGISTRATION_COMPLETE'] ?></div>
        <button type="button" class="close" onclick="Application.Alert.close('alert')">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
<?php endif; ?>

<?php ContentDecorator::begin($this, 'decorators/modal', [
    'title' => $this->messages['MODAL_LOGIN'],
    'header' => $this->renderTemplate('misc/language', ['action' => 'login']),
]) ?>
<form action="/?action=login" method="post" id="<?= $formId ?>">
    <?= $this->renderTemplate('forms/field-text', [
        'formId' => $formId,
        'name' => 'email',
        'type' => 'email',
        'label' => $this->messages['LABEL_EMAIL'],
        'placeholder' => $this->messages['PLACEHOLDER_EMAIL'],
        'value' => $form->email,
        'error' => $form->getErrors()->getItem('email'),
    ]) ?>
    <?= $this->renderTemplate('forms/field-text', [
        'formId' => $formId,
        'name' => 'password',
        'type' => 'password',
        'label' => $this->messages['LABEL_PASSWORD'],
        'placeholder' => $this->messages['PLACEHOLDER_PASSWORD'],
        'value' => '',
        'error' => $form->getErrors()->getItem('password'),
    ]) ?>
    <div class="form-group">
        <button type="submit" class="btn btn-primary"><?= $this->messages['BUTTON_LOGIN'] ?></button>
        <a class="btn btn-link btn-sm" href="/?action=registration"><?= $this->messages['ACTION_REGISTRATION'] ?></a>
    </div>
</form>
<?php ContentDecorator::end() ?>
