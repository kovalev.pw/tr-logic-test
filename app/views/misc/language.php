<?php

use TRLogic\Web\Application;
use TRLogic\Web\View;

/**
 * Шаблон переключателя языка
 *
 * @var View $this
 * @var string $action Идентификатор действия
 */

$i18n = Application::getInstance()->getI18n();
$languages = $i18n->getLanguages();
$current = $lang = $i18n->getCurrentLang();
$languages = array_walk($languages, function (string $language) use ($current, &$lang) {
    if ($language != $current) {
        $lang = $language;
    }
});

?>
<a href="/?action=language&amp;lang=<?= $lang ?>&amp;return=<?= $action; ?>" class="small font-weight-bold">
    <?= View::encode($i18n->getMessages($lang)['LANGUAGE_NAME']) ?>
</a>
