(function() {
    "use strict";

    window.Application = {};

    var Validator = (function (message) {
        var validator = { message: message };

        validator.getMessage = function() {
            return this.message;
        };

        return validator;
    });

    Application.Validators = {
        StringLengthValidator: (function (message, minLength, maxLength) {
            var validator = Validator.call(this, message);

            validator.validateInput = function (element) {
                return (element.value.length >= minLength && element.value.length <= maxLength);
            };

            return validator;
        }),

        CompareValidator: (function (message, targetAttribute) {
            var validator = Validator.call(this, message);

            validator.validateInput = function (element) {
                var value = element.form.querySelector('[name="' + targetAttribute + '"]').value;

                return (value === element.value);
            };

            return validator;
        }),

        RegexValidator: (function (message, rule) {
            var validator = Validator.call(this, message);
            rule = eval(rule);

            validator.validateInput = function (element) {
                return element.value.match(rule);
            };

            return validator;
        }),

        UploadedFileValidator: (function (invalidTypeMessage, invalidSizeMessage, types, maxFileSize) {
            var validator = Validator.call(this, '');

            validator.validateInput = function (element) {
                if (element.files.length < 1) {
                    return true;
                }

                if (types.indexOf(element.files[0].type) === -1) {
                    validator.message = invalidTypeMessage;

                    return false;
                }

                if (element.files[0].size > maxFileSize) {
                    validator.message = invalidSizeMessage;

                    return false;
                }

                return true;
            };

            return validator;
        })
    };

    var validateInput = function (element, validatorsData) {
        var attribute = element.getAttribute('name');
        var error = '';

        if (validatorsData[attribute] !== undefined) {
            validatorsData[attribute].forEach(function (params) {
                var validator = Application.Validators[params[0]].apply(this, params[1]);

                if (!error.length && !validator.validateInput(element)) {
                    error = validator.getMessage();
                }
            });
        }

        document.getElementById(element.id + '-error').textContent = error;

        return (error.length < 1);
    };

    Application.Form = {
        hookSubmit: function (formId, validatorsData) {
            var form = document.getElementById(formId);

            form.onsubmit = function () {
                var formValid = true;

                form.querySelectorAll('[name]').forEach(function (element) {
                    if (!validateInput(element, validatorsData)) {
                        formValid = false;
                    }
                });

                return formValid;
            };

            form.querySelectorAll('[name]').forEach(function (element) {
                element.onblur = element.onchange = element.onkeyup = function () {
                    validateInput(element, validatorsData);

                    return true;
                };
            });

            form.querySelectorAll('[type=file]').forEach(function (element) {
                element.onchange = function (event) {
                    var label = element.parentNode.querySelector('label');

                    label.textContent = (event.target.files.length)
                        ? event.target.files[0].name
                        : label.getAttribute('data-content');

                    validateInput(element, validatorsData);

                    return true;
                };
            });
        },
        
        resetInput: function (inputId) {
            var element = document.getElementById(inputId);

            element.value = '';
            element.dispatchEvent(new Event('change'));
        }
    };

    Application.Alert = {
        close: function (id) {
            var element = document.getElementById(id);

            element.parentNode.removeChild(element);
        }
    }
})();
