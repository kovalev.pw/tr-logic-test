<?php

if (getenv('APP_TEST_MODE') !== 'on') {
    die('You are not allowed to access this file.');
}

ini_set('display_errors', false);
error_reporting(E_ALL);

require_once __DIR__ . '/../c3.php';
require_once __DIR__ . '/../vendor/autoload.php';
$config = require __DIR__ . '/../config/test.php';

TRLogic\Web\Application::run($config);
