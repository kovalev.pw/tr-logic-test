<?php

ini_set('display_errors', (getenv('APP_DEV_MODE') === 'on'));

error_reporting(E_ALL);

require_once __DIR__ . '/../vendor/autoload.php';
$config = require_once __DIR__ . '/../config/web.php';

TRLogic\Web\Application::run($config);
