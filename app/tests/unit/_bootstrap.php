<?php

namespace TRLogic\Models
{
    /**
     * Эмитирует работу нативной функции. Копирует файл в новое назначение
     *
     * @param string $filename Путь исходного файла
     * @param string $destination Путь копии файла
     * @return bool
     * @see \move_uploaded_file()
     */
    function move_uploaded_file(string $filename, string $destination) : bool
    {
        return copy($filename, $destination);
    }
}

namespace tests
{
    use TRLogic\Models\Identity as IdentityBase;
    use TRLogic\Web\IdentityInterface;

    /**
     * Class Identity
     */
    class Identity extends IdentityBase
    {
        /** @var int Идентификатор пользователя */
        public const USER_ID = 1;
        /** @var string Пароль пользователя */
        private const PASSWORD = 'qwerty123';

        /**
         * @inheritDoc
         */
        public static function findById(int $id) : ?IdentityInterface
        {
            static $passwordHash;

            if ($passwordHash === null) {
                $passwordHash =  password_hash(self::PASSWORD, PASSWORD_DEFAULT);
            }

            if ($id == self::USER_ID) {
                return new self([
                    'user_id' => self::USER_ID,
                    'password_hash' => $passwordHash,
                ]);
            }

            return null;
        }
    }
}
