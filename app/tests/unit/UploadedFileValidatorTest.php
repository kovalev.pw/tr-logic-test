<?php

namespace tests\unit;

use tests\UnitTester;
use Codeception\Test\Unit;
use TRLogic\Models\UploadedFile;
use TRLogic\Validators\UploadedFileValidator;

/**
 * Class UploadedFileValidatorTest
 *
 * Выполняет тестирование валидатора загруженного файла
 */
class UploadedFileValidatorTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @var UploadedFileValidator Экземпляр валидатора
     */
    private $validator;

    /**
     * @var string Значение ключа массива $_FILES
     */
    private $key = 'test_file';

    /**
     * @inheritDoc
     */
    protected function _before()
    {
        $this->tester->fillFiles($this->key);
        $this->validator = new UploadedFileValidator(
            'errorUpload',
            'errorType',
            'errorSize',
            ['image/png'],
            pow(1024, 2)
        );
    }

    /**
     * Тестировать валидный файл
     *
     * @return void
     */
    public function testValidFile() : void
    {
        $uploadedFile = new UploadedFile($this->key, codecept_output_dir());
        $this->tester->assertTrue($this->validator->validateValue($uploadedFile));
        $this->tester->assertEmpty($this->validator->getErrorMessage());
        $this->tester->assertNotEmpty($this->validator->getValidatorData());
    }

    /**
     * Тестировать работу при отсутствие файла
     *
     * @return void
     */
    public function testEmptyFile() : void
    {
        $_FILES = [];
        $uploadedFile = new UploadedFile($this->key, codecept_output_dir());
        $this->tester->assertTrue($this->validator->validateValue($uploadedFile));
        $this->tester->assertEmpty($this->validator->getErrorMessage());
    }

    /**
     * Тестировать ошибку частичной загрузки файла
     *
     * @return void
     */
    public function testUploadError() : void
    {
        $_FILES[$this->key]['error'] = UPLOAD_ERR_PARTIAL;
        $uploadedFile = new UploadedFile($this->key, codecept_output_dir());
        $this->tester->assertFalse($this->validator->validateValue($uploadedFile));
        $this->tester->assertEquals($this->validator->getErrorMessage(), 'errorUpload');
    }

    /**
     * Тестировать превышающий лимит размер файла
     *
     * @return void
     */
    public function testInvalidSize() : void
    {
        $_FILES[$this->key]['size'] = PHP_INT_MAX;
        $uploadedFile = new UploadedFile($this->key, codecept_output_dir());
        $this->tester->assertFalse($this->validator->validateValue($uploadedFile));
        $this->tester->assertEquals($this->validator->getErrorMessage(), 'errorSize');
    }

    /**
     * Тестировать не верный тип загруженного файла
     *
     * @return void
     */
    public function testInvalidType() : void
    {
        $_FILES[$this->key]['tmp_name'] = codecept_data_dir() . '/invalid-avatar.ico';
        $uploadedFile = new UploadedFile($this->key, codecept_output_dir());
        $this->tester->assertFalse($this->validator->validateValue($uploadedFile));
        $this->tester->assertEquals($this->validator->getErrorMessage(), 'errorType');
    }
}
