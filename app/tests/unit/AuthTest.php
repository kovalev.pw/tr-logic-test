<?php

namespace tests\unit;

use Codeception\Test\Unit;
use Error;
use tests\Identity;
use tests\UnitTester;
use TRLogic\Web\IdentityInterface;
use TRLogic\Web\Auth;

/**
 * Class AuthTest
 *
 * Тестировать класс авторизации пользователя
 */
class AuthTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @inheritDoc
     */
    public function _before()
    {
        $this->tester->initSession();
    }

    /**
     * Тестировать авторизованную сессию
     *
     * @return void
     */
    public function testUserSession() : void
    {
        $user = new Auth(Identity::class);
        $this->tester->assertFalse($user->isGuest());
        $this->tester->assertNotNull($user->getIdentity());
        $this->tester->assertInstanceOf(IdentityInterface::class, $user->getIdentity());
    }

    /**
     * Тестировать неверные параметры сессии
     *
     * @return void
     */
    public function testInvalidParams() : void
    {
        $noScalar = ['any'];
        $_SESSION['user']['hash'] = md5('any');
        $invalidValues = [
            $_SESSION['user'], // Не верный хеш
            'invalid', // Не является массивом
            ['id' => $noScalar, 'hash' => $noScalar, 'validated_at' => $noScalar], // Не скалярное значение атрибутов
            ['id' => Identity::USER_ID, 'validated_at' => gmdate('U')], // Не полный массив параметров
        ];

        array_walk($invalidValues, function ($value) {
            $_SESSION['user'] = $value;
            $user = new Auth(Identity::class);
            $this->tester->assertTrue($user->isGuest());
            $this->tester->assertNull($user->getIdentity());
            $this->tester->assertArrayNotHasKey('user', $_SESSION);
        });
    }

    /**
     * Тестировать просроченную сессию
     *
     * @return void
     */
    public function testExpiredSession() : void
    {
        $_SESSION['user']['validated_at'] = gmdate('U', time() - UnitTester::SESSION_LIFETIME);
        $user = new Auth(Identity::class);
        $this->tester->assertTrue($user->isGuest());
        $this->tester->assertNull($user->getIdentity());
    }

    /**
     * Тестировать неверную конфигурацию сессий
     *
     * @return void
     */
    public function testInvalidLifetime() : void
    {
        session_abort();
        $this->tester->initSession(0);
        $this->tester->expectThrowable(Error::class, function() {
            new Auth(Identity::class);
        });
    }
}
