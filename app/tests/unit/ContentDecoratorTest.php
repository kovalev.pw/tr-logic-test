<?php

namespace tests\unit;

use tests\UnitTester;
use Codeception\Test\Unit;
use Error;
use TRLogic\Web\ContentDecorator;
use TRLogic\Web\View;

/**
 * Class ContentDecoratorTest
 *
 * Выполняет тестирование класса декоратора содержимого
 */
class ContentDecoratorTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * Тестировать работу фатальных ошибок
     *
     * @return void
     */
    public function testExceptions() : void
    {
        $this->tester->expectThrowable(Error::class, function() {
            ContentDecorator::end();
        });

        $this->tester->expectThrowable(Error::class, function() {
            $view = new View(codecept_data_dir());
            ContentDecorator::begin($view, 'template');
            ContentDecorator::end();
            ContentDecorator::end();
        });

        $this->tester->expectThrowable(Error::class, function() {
            $view = new View(codecept_data_dir());
            ContentDecorator::begin($view, 'template');
            ContentDecorator::begin($view, 'template');
        });
    }
}
