<?php

namespace tests\unit;

use tests\UnitTester;
use Codeception\Test\Unit;
use Error;
use TRLogic\Models\UploadedFile;

/**
 * Class UploadedFileTest
 *
 * Выполняет тестирование экземпляра класса загруженного файла
 */
class UploadedFileTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @var string Путь назначения копируемого файла
     */
    private $destination;

    /**
     * @var string Значение ключа массива $_FILES
     */
    private $key = 'test_file';

    /**
     * @inheritDoc
     */
    protected function _before()
    {
        $this->tester->fillFiles($this->key);
        $this->destination = codecept_output_dir() . '/copied.file';
    }

    /**
     * @inheritDoc
     */
    protected function _after()
    {
        if (file_exists($this->destination)) {
            unlink($this->destination);
        }
    }

    /**
     * Тестировать интерфейс экземпляра класса загруженного файла
     *
     * @return void
     */
    public function testInterface() : void
    {
        $uploadedFile = new UploadedFile($this->key, codecept_output_dir());
        $this->tester->assertTrue($uploadedFile->isFileReceived());
        $this->tester->assertFalse($uploadedFile->hasError());
        $this->tester->assertEquals($uploadedFile->getError(), $_FILES[$this->key]['error']);
        $this->tester->assertEquals($uploadedFile->getMimeType(), $_FILES[$this->key]['type']);
        $this->tester->assertEquals($uploadedFile->getSize(), $_FILES[$this->key]['size']);
    }

    /**
     * Тестировать работу при отсутствие файла
     *
     * @return void
     */
    public function testEmptyFile() : void
    {
        $_FILES[$this->key]['error'] = UPLOAD_ERR_NO_FILE;
        $uploadedFile = new UploadedFile($this->key, codecept_output_dir());
        $this->tester->assertFalse($uploadedFile->isFileReceived());
        $this->tester->assertTrue($uploadedFile->hasError());
    }

    /**
     * Тестировать копирование загруженного файла в путь назначения
     *
     * @return void
     */
    public function testCopyToDestination() : void
    {
        $uploadedFile = new UploadedFile($this->key, codecept_output_dir());
        $uploadedFile->copyToDestination($this->destination);
        $this->tester->assertFileExists($this->destination);

        $this->tester->expectThrowable(Error::class, function() {
            $_FILES[$this->key]['error'] = UPLOAD_ERR_NO_FILE;
            $uploadedFile = new UploadedFile($this->key, codecept_output_dir());
            $uploadedFile->copyToDestination(codecept_output_dir());
        });
    }

    /**
     * Тестировать фатальные ошибки, возникающие при загрузки файла
     *
     * @return void
     */
    public function testFatalErrors() : void
    {
        foreach ([UPLOAD_ERR_NO_TMP_DIR, UPLOAD_ERR_CANT_WRITE, UPLOAD_ERR_EXTENSION] as $fatalError) {
            $this->tester->expectThrowable(Error::class, function() use ($fatalError) {
                $_FILES[$this->key]['error'] = $fatalError;
                new UploadedFile($this->key, codecept_output_dir());
            });
        }
    }

    /**
     * Тестировать фатальную ошибку, возникающую при перемещение файла из временной папки
     *
     * @return void
     */
    public function testMoveUploadedFile() : void
    {
        $this->tester->expectThrowable(Error::class, function() {
            $_FILES[$this->key]['tmp_name'] = codecept_data_dir() . '/invalid.file';
            new UploadedFile($this->key, codecept_output_dir());
        });
    }
}
