<?php

namespace tests\unit;

use tests\UnitTester;
use Codeception\Test\Unit;
use TRLogic\Validators\CompositeValidator;
use TRLogic\Validators\EmailNormalizer;
use TRLogic\Validators\EmailValidator;
use TRLogic\Validators\ExistsValidator;
use TRLogic\Validators\RegexValidator;
use TRLogic\Validators\CompareValidator;
use TRLogic\Validators\StringLengthValidator;
use TRLogic\Validators\StringNormalizer;
use TRLogic\Validators\ValidatorDataInterface;
use TRLogic\Validators\ValidatorInterface;

/**
 * Class ValidatorTest
 *
 * Выполняет тестирование валидаторов
 */
class ValidatorTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @var string[] Не скалярное значение
     */
    private $noScalar = ['key' => 'value'];

    /**
     * Тестировать нормализатор email адресов
     *
     * @return void
     */
    public function testEmailNormalizer() : void
    {
        $normalizer = new EmailNormalizer();
        $this->tester->assertEquals($normalizer->normalizeValue($this->noScalar), $this->noScalar);
        $this->tester->assertIsString($normalizer->normalizeValue(null));
        $this->tester->assertEquals($normalizer->normalizeValue('UPPERCASE@EXAMPLE.COM'), 'uppercase@example.com');
    }

    /**
     * Тестировать нормализатор строк
     *
     * @return void
     */
    public function testStringNormalizer() : void
    {
        $normalizer = new StringNormalizer();
        $this->tester->assertEquals($normalizer->normalizeValue($this->noScalar), $this->noScalar);
        $this->tester->assertIsString($normalizer->normalizeValue(null));
        $this->tester->assertEquals($normalizer->normalizeValue(" \t"), '');
        $this->tester->assertEquals($normalizer->normalizeValue("1 \n\n2 \t 3\r"), '1 2 3');
    }

    /**
     * Тестировать валидатор строк
     *
     * @return void
     */
    public function testStringLengthValidator() : void
    {
        $validator = new StringLengthValidator('error', 2, 3);
        $this->tester->assertFalse($validator->validateValue('1'));
        $this->tester->assertTrue($validator->validateValue('12'));
        $this->tester->assertTrue($validator->validateValue('123'));
        $this->tester->assertFalse($validator->validateValue('1234'));
    }

    /**
     * Тестировать валидаторы
     *
     * @return void
     */
    public function testCheckValidators() : void
    {
        $value = 'test@example.com';
        $invalid = 'invalid';
        $errorMessage = 'error';
        $validators = [
            new CompareValidator($errorMessage, 'targetAttribute', $value),
            new EmailValidator($errorMessage),
            new RegexValidator($errorMessage, '/^test\@example\.com$/'),
            new StringLengthValidator('error', strlen($value), strlen($value)),
            new ExistsValidator($errorMessage, [$value]),
        ];

        $validators[] = new CompositeValidator($validators);

        array_walk($validators, function (ValidatorInterface $validator) use ($value, $invalid, $errorMessage) {
            $this->tester->assertFalse($validator->validateValue($this->noScalar));
            $this->tester->assertTrue($validator->validateValue($value));
            $this->tester->assertFalse($validator->validateValue($invalid));
            $this->tester->assertEquals($validator->getErrorMessage(), $errorMessage);

            if ($validator instanceof ValidatorDataInterface) {
                $this->tester->assertNotEmpty($validator->getValidatorData());
            }
        });
    }
}
