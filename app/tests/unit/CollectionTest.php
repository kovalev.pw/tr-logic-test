<?php

namespace tests\unit;

use Codeception\Test\Unit;
use tests\UnitTester;
use TRLogic\Collections\Collection;

/**
 * Class CollectionTest
 *
 * Тестирует класс коллекции
 */
class CollectionTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @return void
     */
    public function testCollection() : void
    {
        $array = ['a' => 1, 'b' => 2];
        $defaultValue = 3;
        $collection = new Collection($array);
        $this->tester->assertIsArray($collection->getItems());
        $this->tester->assertEquals($collection->getItems(), $array);
        $this->tester->assertEquals($collection->getItem('a'), $array['a']);
        $this->tester->assertEquals($collection->getItem('c', $defaultValue), $defaultValue);

        foreach ($collection as $key => $value) {
            $this->tester->assertTrue(array_key_exists($key, $array));
            $this->tester->assertEquals($value, $array[$key]);
            $this->tester->assertEquals($collection->getItem($key), $array[$key]);
            $collection->removeItem($key);
        }

        $this->tester->assertTrue($collection->isEmpty());
        $collection->setItem('c', $defaultValue);
        $this->tester->assertFalse($collection->isEmpty());
        $this->tester->assertTrue($collection->hasItem('c'));
        $collection->removeItem('c');
        $this->tester->assertFalse($collection->hasItem('c'));
    }
}
