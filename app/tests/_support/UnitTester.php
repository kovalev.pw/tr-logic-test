<?php

namespace tests;

use tests\_generated\UnitTesterActions;
use Codeception\Actor;
use TRLogic\Web\Auth;

/**
 * Class UnitTester
 */
class UnitTester extends Actor
{
    use UnitTesterActions;

    /**
     * @var int Время жизни сессии по умолчанию
     */
    public const SESSION_LIFETIME = 3600;

    /**
     * Наполнить массив $_FILES
     *
     * @param string $key Имя ключа массива
     * @return void
     */
    public function fillFiles(string $key) : void
    {
        global $_FILES;

        $path = codecept_data_dir() . '/valid-avatar.png';
        $_FILES = [
            $key => [
                'error' => UPLOAD_ERR_OK,
                'name' => 'valid-avatar.png',
                'size' => filesize($path),
                'tmp_name' => $path,
                'type' => 'image/png',
            ],
        ];
    }

    /**
     * Инициализировать авторизованную сессию
     *
     * @param int $lifetime Время жизни сессии
     */
    public function initSession($lifetime = self::SESSION_LIFETIME) : void
    {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_start(['cookie_lifetime' => $lifetime]);
        }

        $identity = Identity::findById(Identity::USER_ID);
        $_SESSION = [
            'user' => [
                'id' => $identity->getId(),
                'hash' => Auth::getValidHash($identity),
                'validated_at' => gmdate('U'),
            ],
        ];
    }
}
