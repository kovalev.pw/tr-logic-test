<?php

namespace tests;

use tests\_generated\FunctionalTesterActions;
use Codeception\Actor;
use Codeception\Util\HttpCode;

/**
 * Class FunctionalTester
 */
class FunctionalTester extends Actor
{
    use FunctionalTesterActions;

    /** @var string */
    public const ACTION_DEFAULT = '/';

    /** @var string */
    public const ACTION_LANGUAGE = '/?action=language';

    /** @var string */
    public const ACTION_LOGIN = '/?action=login';

    /** @var string */
    public const ACTION_LOGOUT = '/?action=logout';

    /** @var string */
    public const ACTION_PROFILE = '/?action=profile';

    /** @var string */
    public const ACTION_REGISTRATION = '/?action=registration';

    /** @var string */
    public const LOGIN_EMAIL = 'test@example.com';

    /** @var string */
    public const LOGIN_PASSWORD = 'qwerty123';

    /** @var string */
    public const FULL_NAME = 'Test user';

    /** @var string */
    public const CREATED_AT = '2001-01-01 21:01:01 UTC';

    /**
     * @param string $email
     * @param string $password
     * @return void
     */
    public function login(string $email = self::LOGIN_EMAIL, string $password = self::LOGIN_PASSWORD) : void
    {
        $this->amOnPage(FunctionalTester::ACTION_LOGIN);
        $this->seeInCurrentUrl(FunctionalTester::ACTION_LOGIN);
        $this->canSeeResponseCodeIs(HttpCode::OK);
        $this->submitForm('form#login', ['email' => $email, 'password' => $password]);
    }

    /**
     * @return void
     */
    public function amOnRegistrationPage() : void
    {
        $this->amOnPage(FunctionalTester::ACTION_REGISTRATION);
        $this->seeInCurrentUrl(FunctionalTester::ACTION_REGISTRATION);
        $this->canSeeResponseCodeIs(HttpCode::OK);
    }
}
