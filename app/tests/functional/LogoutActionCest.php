<?php

namespace tests\functional;

use tests\FunctionalTester;
use Codeception\Util\HttpCode;

/**
 * Class LogoutActionCest
 *
 * Выполняет тестирование страницы выход пользователя
 */
class LogoutActionCest
{
    /**
     * Тестировать окончание сессии авторизованного пользователя
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testLogoutAction(FunctionalTester $tester) : void
    {
        $tester->login();
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_PROFILE);
        $tester->canSeeResponseCodeIs(HttpCode::OK);

        $tester->amOnPage(FunctionalTester::ACTION_LOGOUT);
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_LOGIN);
        $tester->canSeeResponseCodeIs(HttpCode::OK);
    }
}
