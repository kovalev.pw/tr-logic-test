<?php

namespace tests\functional;

use tests\FunctionalTester;
use Codeception\Util\HttpCode;

/**
 * Class RegistrationActionCest
 *
 * Выполняет тестирование страницы регистрации
 */
class RegistrationActionCest
{
    /** @var string[] Коллекция типов полей формы */
    private const FIELD_TYPES = [
        'full_name' => 'text',
        'email' => 'email',
        'password' => 'password',
        'password_repeat' => 'password',
        'avatar' => 'file',
    ];

    /**
     * @var string[] Коллекция значений полей формы
     */
    private $user;

    /**
     * RegistrationActionCest constructor.
     */
    public function __construct()
    {
        $this->user = [
            'full_name' => 'Random user',
            'email' => dechex(microtime(true) * 10000) . '@example.com',
            'password' => 'qwerty123',
            'password_repeat' => 'qwerty123',
        ];
    }

    /**
     * Тестировать работу элементов страницы
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testRegistrationAction(FunctionalTester $tester) : void
    {
        $tester->amOnRegistrationPage();
        $tester->seeElement('form#registration');
        $tester->seeElement('#registration button[type=submit]');
        $tester->seeElement(sprintf('#registration a[href="%s"]', FunctionalTester::ACTION_LOGIN));
        $tester->seeElement(sprintf('a[href="%s&lang=en&return=registration"]', FunctionalTester::ACTION_LANGUAGE));

        foreach (self::FIELD_TYPES as $name => $type) {
            $tester->seeElement(sprintf('#registration input[name=%s][type=%s]', $name, $type));
            $tester->see('', sprintf('#registration-%s-error', $name));
        }
    }

    /**
     * Тестировать форму с незаполненными полями
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testRegistrationForm(FunctionalTester $tester) : void
    {
        $tester->amOnRegistrationPage();
        $tester->attachFile('#registration input[name=avatar]', 'invalid-avatar.ico');
        $tester->submitForm('#registration', [
            'full_name' => '',
            'email' => '',
            'password' => '',
            'password_repeat' => 'any',
        ]);
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_REGISTRATION);

        foreach (self::FIELD_TYPES as $name => $type) {
            $tester->seeElement(sprintf('//*[@id="registration-%s-error" and text()]', $name));
        }
    }

    /**
     * Тестировать форму с валидными значениями полей
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testRegistration(FunctionalTester $tester) : void
    {
        $tester->amOnRegistrationPage();
        $tester->attachFile('#registration input[name=avatar]', 'valid-avatar.png');
        $tester->submitForm('#registration', $this->user);

        $tester->seeInCurrentUrl(FunctionalTester::ACTION_LOGIN);
        $tester->seeElement('//*[@id="alert" and text()]');

        $tester->amOnPage(FunctionalTester::ACTION_LOGIN);
        $tester->canSeeResponseCodeIs(HttpCode::OK);
        $tester->dontSeeElement('//*[@id="alert" and text()]');
    }

    /**
     * Тестировать регистрацию уже зарегистрированного пользователя
     *
     * @param FunctionalTester $tester
     */
    public function testExistsEmail(FunctionalTester $tester) : void
    {
        $tester->amOnRegistrationPage();
        $tester->submitForm('#registration', $this->user);
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_REGISTRATION);

        foreach (self::FIELD_TYPES as $name => $type) {
            if ($name == 'email') {
                $tester->seeElement('//*[@id="registration-email-error" and text()]');
            } else {
                $tester->see('', sprintf('#registration-%s-error', $name));
            }
        }
    }

    /**
     * Тестировать наличие аватара на странице профиля
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testProfileAvatar(FunctionalTester $tester) : void
    {
        $tester->login($this->user['email'], $this->user['password']);
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_PROFILE);
        $tester->canSeeResponseCodeIs(HttpCode::OK);
        $tester->seeElement('//img[@class="img-fluid rounded" and contains(@src, "/assets/avatars/")]');
    }

    /**
     * Тестировать запрет регистрации для авторизованного пользователя
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testForbiddenError(FunctionalTester $tester) : void
    {
        $tester->login($this->user['email'], $this->user['password']);
        $tester->amOnPage(FunctionalTester::ACTION_REGISTRATION);
        $tester->canSeeResponseCodeIs(HttpCode::FORBIDDEN);
    }
}
