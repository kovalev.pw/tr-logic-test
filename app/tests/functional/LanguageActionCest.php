<?php

namespace tests\functional;

use tests\FunctionalTester;
use Codeception\Util\HttpCode;

/**
 * Class LanguageActionCest
 *
 * Выполняет тестирование смены языка пользователя
 */
class LanguageActionCest
{
    /**
     * Тестировать смену языка на русский
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testRussianLang(FunctionalTester $tester) : void
    {
        $tester->amOnPage(FunctionalTester::ACTION_LANGUAGE . '&lang=ru&return=registration');
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_REGISTRATION);
        $tester->canSeeResponseCodeIs(HttpCode::OK);
        $tester->seeElement('//h5[@class="modal-title" and contains(text(), "Зарегистрироваться")]');
        $tester->seeElement('html[lang=ru]');
    }

    /**
     * Тестировать смену языка на английский
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testEnglishLang(FunctionalTester $tester) : void
    {
        $tester->amOnPage(FunctionalTester::ACTION_LANGUAGE . '&lang=en&return=registration');
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_REGISTRATION);
        $tester->canSeeResponseCodeIs(HttpCode::OK);
        $tester->seeElement('//h5[@class="modal-title" and contains(text(), "Sign up")]');
        $tester->seeElement('html[lang=en]');
    }

    /**
     * Тестировать смену языка на несуществующий
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testInvalidLang(FunctionalTester $tester) : void
    {
        $page = FunctionalTester::ACTION_LANGUAGE . '&lang=fr';
        $tester->amOnPage($page);
        $tester->seeResponseCodeIs(HttpCode::NOT_FOUND);
    }
}
