<?php

namespace tests\functional;

use tests\FunctionalTester;
use Codeception\Util\HttpCode;

/**
 * Class ProfileActionCest
 *
 * Выполняет тестирование страницы профиля
 */
class ProfileActionCest
{
    /**
     * Тестирует работу страницы профиля пользователя
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testProfileAction(FunctionalTester $tester) : void
    {
        $tester->amOnPage(FunctionalTester::ACTION_PROFILE);
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_LOGIN);
        $tester->canSeeResponseCodeIs(HttpCode::OK);

        $tester->login();
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_PROFILE);
        $tester->canSeeResponseCodeIs(HttpCode::OK);

        $tester->see(FunctionalTester::FULL_NAME);
        $tester->see(FunctionalTester::LOGIN_EMAIL);
        $tester->see(FunctionalTester::CREATED_AT);
    }
}
