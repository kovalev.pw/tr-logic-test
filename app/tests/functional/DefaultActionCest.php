<?php

namespace tests\functional;

use tests\FunctionalTester;
use Codeception\Util\HttpCode;

/**
 * Class DefaultActionCest
 *
 * Выполняет тестирование страницы по умолчанию
 */
class DefaultActionCest
{
    /**
     * Тестировать работу перенаправлений
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testDefaultAction(FunctionalTester $tester) : void
    {
        $tester->amOnPage(FunctionalTester::ACTION_DEFAULT);
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_LOGIN);
        $tester->canSeeResponseCodeIs(HttpCode::OK);
        $tester->login();
        $tester->amOnPage(FunctionalTester::ACTION_DEFAULT);
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_PROFILE);
        $tester->canSeeResponseCodeIs(HttpCode::OK);
    }

    /**
     * Тестировать запрос несуществующего адреса страницы
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testNotFoundError(FunctionalTester $tester) : void
    {
        $tester->amOnPage('/?action=unknown');
        $tester->canSeeResponseCodeIs(HttpCode::NOT_FOUND);
        $tester->amOnPage('/unknown');
        $tester->canSeeResponseCodeIs(HttpCode::NOT_FOUND);
    }
}
