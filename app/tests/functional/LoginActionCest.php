<?php

namespace tests\functional;

use tests\FunctionalTester;
use Codeception\Util\HttpCode;

/**
 * Class LoginActionCest
 *
 * Выполняет тестирование страницы входа
 */
class LoginActionCest
{
    /** @var string[] Типы полей формы */
    private const FIELD_TYPES = [
        'email' => 'email',
        'password' => 'password',
    ];

    /**
     * Тестировать работу элементов страницы
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testLoginAction(FunctionalTester $tester) : void
    {
        $tester->amOnPage(FunctionalTester::ACTION_LOGIN);
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_LOGIN);
        $tester->canSeeResponseCodeIs(HttpCode::OK);
        $tester->seeElement('form#login');
        $tester->seeElement('#login button[type=submit]');
        $tester->seeElement(sprintf('#login a[href="%s"]', FunctionalTester::ACTION_REGISTRATION));
        $tester->seeElement(sprintf('a[href="%s&lang=en&return=login"]', FunctionalTester::ACTION_LANGUAGE));

        foreach (self::FIELD_TYPES as $name => $type) {
            $tester->seeElement(sprintf('#login input[name=%s][type=%s]', $name, $type));
            $tester->see('', sprintf('#login-%s-error', $name));
        }
    }

    /**
     * Тестировать работу формы с незаполненными полями
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testLoginForm(FunctionalTester $tester) : void
    {
        $tester->login('', '');
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_LOGIN);

        foreach (self::FIELD_TYPES as $name => $type) {
            $tester->seeElement(sprintf('//*[@id="login-%s-error" and text()]', $name));
        }
    }

    /**
     * Тестировать форму входа с неверными данными
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testInvalidCredentials(FunctionalTester $tester) : void
    {
        $tester->login('invalid', 'short');
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_LOGIN);
        $tester->seeElement('//*[@id="login-email-error" and text()]');
        $tester->seeElement('//*[@id="login-password-error" and text()]');
    }

    /**
     * Тестировать форму входа с данными несуществующего пользователя
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testUnregisteredEmail(FunctionalTester $tester) : void
    {
        $tester->login('invalid@example.com');
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_LOGIN);
        $tester->seeElement('//*[@id="login-email-error" and text()]');
        $tester->see('', '#login-password-error');
    }

    /**
     * Тестировать форму входа с неверным паролем
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testInvalidPassword(FunctionalTester $tester) : void
    {
        $tester->login(FunctionalTester::LOGIN_EMAIL, 'invalidPass');
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_LOGIN);
        $tester->seeElement('//*[@id="login-password-error" and text()]');
        $tester->see('', '#login-email-error');
    }

    /**
     * тестировать форму входа с верными учётными данными
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testLogin(FunctionalTester $tester) : void
    {
        $tester->login();
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_PROFILE);
        $tester->canSeeResponseCodeIs(HttpCode::OK);
    }

    /**
     * Тестировать перенаправление со страницы уже авторизованного пользователя
     *
     * @param FunctionalTester $tester
     * @return void
     */
    public function testRedirectToProfile(FunctionalTester $tester) : void
    {
        $tester->login();
        $tester->amOnPage(FunctionalTester::ACTION_LOGIN);
        $tester->seeInCurrentUrl(FunctionalTester::ACTION_PROFILE);
        $tester->canSeeResponseCodeIs(HttpCode::OK);
    }
}
