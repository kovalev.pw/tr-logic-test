ARG UID=33
ARG GID=33
# Base image
FROM php:7.4-fpm as base

ENV COMPOSER_HOME /tmp

ADD https://getcomposer.org/installer /tmp/composer-setup.php

RUN apt-get update && apt-get install -y --no-install-recommends \
        unzip \
        libmemcached-dev \
        zlib1g-dev \
        libicu-dev \
    && apt-get purge -y --auto-remove \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install intl \
    && pecl install memcached-3.1.5 \
    && docker-php-ext-enable memcached \
    && docker-php-source delete \
    && cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini \
    && sed -i 's/short_open_tag = Off/short_open_tag = On/' /usr/local/etc/php/php.ini \
    && sed -i 's/expose_php = On/expose_php = Off/' /usr/local/etc/php/php.ini \
    && php /tmp/composer-setup.php --install-dir=/usr/bin --filename=composer --version=1.10.5

RUN php /tmp/composer-setup.php --install-dir=/usr/bin --filename=composer --version=1.10.5

WORKDIR /app

EXPOSE 9000/tcp

# Debug image
FROM base as debug

ARG UID
ARG GID

ENV APP_DEV_MODE=on
ENV APP_TEST_MODE=on

RUN pecl install xdebug-2.9.4 \
    && docker-php-ext-enable xdebug \
    && docker-php-source delete

USER $UID:$GID

# Production image
FROM base as production

ARG UID
ARG GID

ENV APP_DEV_MODE=off
ENV APP_TEST_MODE=off

COPY ./app /app

RUN composer install --no-dev \
    && chown -R $UID:$GID /app

USER $UID:$GID

VOLUME ["/app/public/assets/avatars"]
